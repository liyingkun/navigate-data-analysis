# --*-- coding: utf-8 _*_
# @Author   :liyingkun
# @time     :2020/5/12 8:57 上午
# @File     :0512fixtiktokind.py
# @Software :PyCharm

""" 修复tiktok industry问题"""
import pymongo
import requests

fromMongoUrl = 'mongodb+srv://navigate-prod-user:KNkt0rq70dlNbSHc@navigate-prod.cktro.mongodb.net/?retryWrites=true&w=majority'
fromClient = pymongo.MongoClient(fromMongoUrl)
fromdbname = 'navigate-prod'


def migrate(fromcollection, tocollection):
    ress = fromcollection.find({"platform": "tiktok"}, {"_id": 1, "kolID": 1, "industry": 1}, no_cursor_timeout=True)
    i = 0
    j = 0
    ress = list(ress)
    for res in ress:
        try:
            i = i + 1
            res['industryType'] = res.get('industry')
            tocollection.update(
                {'_id': res.get('_id')},
                {'$set': res},
                True)
            print(f'{i},{res.get("_id")}')
        except Exception as e:
            j = j + 1
            print(res.get('_id'), res.get('kolID'))
    ress.close()
    print(f'========同步完成，共同步数据{i},失败{j}')

collections=["mid_influencertwodminfo"]
for collection in collections:
    try:
        print(f'开始同步{collection}的数据')
        fromcollection = fromClient[fromdbname][collection]
        # tocollection = toClient[todbname][collection]
        migrate(fromcollection, fromcollection)
        print(f'collention::: {collection} 同步完成')
    except Exception as e:
        print(f'{str(e)}')
