# --*-- coding: utf-8 _*_
# @Author   :liyingkun
# @time     :2020/8/5 3:19 下午
# @File     :0805dataforyx.py
# @Software :PyCharm

""" 导出联系方式 """

import pymongo

fromMongoUrl = 'mongodb://192.168.1.27:27017'
fromClient = pymongo.MongoClient(fromMongoUrl)

fromdbname = 'navigate-dev'

data = fromClient[fromdbname]['mid_influencertwodminfo'].find({"isContact": True, "areaCode": {"$ne": "CN"},
                                                               "platform": {"$in": ["youtube", "tiktok", "instagram"]}
                                                               },
                                                              {"_id": 0, "displayName": 1, "fans": 1, "platform": 1,
                                                               "email": 1, "phone": 1, "area": 1, "areaCode": 1,
                                                               "platformLink": 1,
                                                               "updateAtUtc": 1})

import pandas as pd
import numpy as np

df = pd.DataFrame(list(data))

df.head(5)
df = df.dropna()

df['fans_cut'] = pd.cut(df.fans, [1000, 10000, 100000, 1000000, np.inf],
                        labels=['B:1k-1w(素人)', 'C:1w-10w(尾部)', 'E:10w-100w(腰部)', 'J:>100w(头部)'])

df.drop("fans", axis=1, inplace=True)

df = df.dropna()
df.head(5)

df.to_excel('kolContact_2020805.xlsx', index=False)
