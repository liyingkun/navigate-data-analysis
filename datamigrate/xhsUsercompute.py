# --*-- coding: utf-8 _*_
# @Author   :liyingkun
# @time     :2020/5/9 10:16 下午
# @File     :xhsUsercompute.py
# @Software :PyCharm

""":arg
计算爬取到的小红书用户数据，将其写入navigate-prod数据库
"""
import json

import pymongo
import requests

fromMongoUrl = 'mongodb://2o5572137z.qicp.vip:48547'
fromClient = pymongo.MongoClient(fromMongoUrl)
fromdbname = 'navigate-spider'

ress = fromClient[fromdbname]['XhsUser'].find({}, {'_id': 0, "userId": 1}, no_cursor_timeout=True)

reslist = list(ress)


def f_compute(platformUID):
    ETLserver = "http://127.0.0.1:7776/modelApi/v2/xiaohongshu"

    payload = {
        "type": "offline",
        "data": [
            {
                "platformUID": platformUID
            }
        ]
    }
    try:
        payload = json.dumps(payload, ensure_ascii=False)
        res = requests.request("POST", ETLserver, data=payload.encode('utf-8'))
        print(res)
    except Exception as e:
        print(f'{str(e)},"---",{platformUID}')


for res in reslist:
    f_compute(res.get('userId'))
