# --*-- coding: utf-8 _*_
# @Author   :liyingkun
# @time     :2020/6/22 11:26 上午
# @File     :0622devupdate.py
# @Software :PyCharm

""" 0622更新本地机房数据库 """

import pymongo

# fromMongoUrl = 'mongodb://2o5572137z.qicp.vip:48547'
fromMongoUrl = 'mongodb://192.168.1.27:27017'
toMongoUrl = 'mongodb://192.168.1.27:27017'
fromClient = pymongo.MongoClient(fromMongoUrl)
toClient = pymongo.MongoClient(toMongoUrl)

fromdbname = 'navigate-dev'
todbname = 'navigate-test'

data = fromClient[fromdbname]['mid_influencertwodminfo'].find({"idUser": None, "claimed": 0}, {"_id": 0, "kolID": 1})
dataLs = list(data)
for i in dataLs:
    fromClient[fromdbname]['mid_influencertwodminfo'].update_one({"kolID": i.get("kolID")},
                                                                 {"$set": {"idUser": i.get("kolID")}}, True)
