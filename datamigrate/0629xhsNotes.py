# --*-- coding: utf-8 _*_
# @Author   :liyingkun
# @time     :2020/6/29 3:46 下午
# @File     :0629xhsNotes.py
# @Software :PyCharm

""" 生产环境 小红书帖子图片加载不出来的问题 """

import pymongo

fromMongoUrl = 'mongodb://root:dnqW9MriFu5P2x@120.76.101.229:27017/?authSource=admin'
toMongoUrl = 'mongodb+srv://navigate-prod-user:KNkt0rq70dlNbSHc@navigate-prod.cktro.mongodb.net/?retryWrites=true&w=majority'
fromClient = pymongo.MongoClient(fromMongoUrl)
toClient = pymongo.MongoClient(toMongoUrl)

fromdbname = 'navigate-test'
todbname = 'navigate-prod'

collections = [
    # 'mid_influencerhotpost',
    # 'mid_influencertwodminfo',
    # 'mid_influencerstatistics',
    # 'mid_similarinfluencer',
    'mid_influencerpost',
]


def migrate(fromcollection, tocollection):
    ress = fromcollection.find({"platform": "xiaohongshu"}, no_cursor_timeout=True)
    i = 0
    j = 0
    for res in ress:
        try:
            i = i + 1
            tocollection.update(
                {'_id': res.get('_id')},
                {'$set': res},
                True)
        except Exception as e:
            j = j + 1
            print(res.get('_id'), res.get('kolID'))
    ress.close()
    print(f'========同步完成，共同步数据{i},失败{j}')


for collection in collections:
    try:
        print(f'开始同步{collection}的数据')
        fromcollection = fromClient[fromdbname][collection]
        tocollection = toClient[todbname][collection]
        migrate(fromcollection, tocollection)
        print(f'collention::: {collection} 同步完成')
    except Exception as e:
        print(f'{str(e)}')
