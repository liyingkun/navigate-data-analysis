# --*-- coding: utf-8 _*_
# @Author   :liyingkun
# @time     :2020/6/16 9:15 上午
# @File     :0616xhsMigrate.py
# @Software :PyCharm

""" 解决小红书帖子同步的问题 """

import pymongo

# fromMongoUrl = 'mongodb://2o5572137z.qicp.vip:48547'
fromMongoUrl = 'mongodb://192.168.1.27:27017'
toMongoUrl = 'mongodb://root:dnqW9MriFu5P2x@120.76.101.229:27017/?authSource=admin'
fromClient = pymongo.MongoClient(fromMongoUrl)
toClient = pymongo.MongoClient(toMongoUrl)

fromdbname = 'navigate-dev'
todbname = 'navigate-test'

# 1.读取to端2dminfo
# 2.检查kolID在post中是否存在，不存在的记录下来
# 3.不存在的从from端查找同步，from端不存在的，记录下来，交给爬虫爬取

kolIDs = toClient[todbname]['mid_influencertwodminfo'].find({"platform": "xiaohongshu"},
                                                            {"_id": 0, "kolID": 1, "platformUID": 1})
kolIDList = list(kolIDs)

noExitKolIDs = []
for item in kolIDList:
    print(item)
    res = toClient[todbname]['mid_influencerpost'].find_one({"kolID": item.get('kolID')}, {"_id": 0, "kolID": 1})
    if not res:
        noExitKolIDs.append(item)
print(noExitKolIDs)

# noExitKolIDs = [{'kolID': 'b4e81a5c-bc31-3f3c-a02e-3aca37127875', "platformUID": '1'}]

CrawlerKolIDs = []
for kolIDObj in noExitKolIDs:
    res = fromClient[fromdbname]['mid_influencerpost'].find({"kolID": kolIDObj.get('kolID')})
    if not res and kolIDObj.get('platformUID'):
        CrawlerKolIDs.append(f"https://www.xiaohongshu.com/user/profile/{kolIDObj.get('platformUID')}")
    else:
        for item in list(res):
            toClient[todbname]['mid_influencerpost'].update_one({"idPost": item.get('idPost')}, {'$set': item}, True)

if CrawlerKolIDs:
    with open('/Users/yingkunli/Desktop/navigate-data-analysis/datamigrate/xhsCrawler.txt', 'w+') as f:
        print(CrawlerKolIDs, file=f)
