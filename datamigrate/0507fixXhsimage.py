# --*-- coding: utf-8 _*_
# @Author   :liyingkun
# @time     :2020/5/7 5:28 下午
# @File     :0507fixXhsimage.py
# @Software :PyCharm

"""
修复小红书的帖子image字段
"""

import pymongo

fromMongoUrl = 'mongodb://2o5572137z.qicp.vip:48547'
toMongoUrl = 'mongodb://root:dnqW9MriFu5P2x@120.76.101.229:27017/?authSource=admin'
fromClient = pymongo.MongoClient(fromMongoUrl)
toClient = pymongo.MongoClient(toMongoUrl)

fromdbname = 'navigate-dev'
todbname = 'navigate-test'

collections = ['mid_influencerpost', ]


def fixbug(fromcollection, tocollection):
    ress = fromcollection.find({"platform": "xiaohongshu"}, no_cursor_timeout=True)
    i = 0
    j = 0
    for res in ress:
        image = res.get('image')
        if '///' in image:
            images = image.split('///')
            res['image'] = "/https://".join(images[0:2])
        try:

            tocollection.update(
                {'_id': res.get('_id')},
                {'$set': res},
                True)
            i = i + 1
            print(f'更新了{i}个')
        except Exception as e:
            j = j + 1
            print(res.get('_id'), res.get('kolID'))
    ress.close()
    print(f'========同步完成，共同步数据{i},失败{j}')


for collection in collections:
    try:
        print(f'开始同步{collection}的数据')
        # fromcollection = fromClient[fromdbname][collection]
        tocollection = toClient[todbname][collection]
        fixbug(tocollection, tocollection)
        print(f'collention::: {collection} 同步完成')
    except Exception as e:
        print(f'str(e')
