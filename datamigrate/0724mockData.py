# --*-- coding: utf-8 _*_
# @Author   :liyingkun
# @time     :2020/7/24 10:25 上午
# @File     :0724mockData.py
# @Software :PyCharm

""" mock数据 """
import pymongo

fromMongoUrl = 'mongodb+srv://navigate-prod-user:KNkt0rq70dlNbSHc@navigate-prod.cktro.mongodb.net/?retryWrites=true&w=majority'
toMongoUrl = 'mongodb://root:dnqW9MriFu5P2x@120.76.101.229:27017/?authSource=admin'
fromClient = pymongo.MongoClient(fromMongoUrl)
toClient = pymongo.MongoClient(toMongoUrl)

fromdbname = 'navigate-prod'
todbname = 'navigate-dev'

collections = [
    'activities',
    'campaigns',
    'jobs',
    'tasks',
    'users',
]


def migrate(fromcollection, tocollection, findCond={}, updateOpt={}):
    ress = fromcollection.find(findCond, no_cursor_timeout=True)
    i = 0
    j = 0
    for res in ress:
        if updateOpt:
            res = res.update(**updateOpt)
        try:
            i = i + 1
            tocollection.update(
                {'_id': res.get('_id')},
                {'$set': res},
                True)
        except Exception as e:
            j = j + 1
            print(res.get('_id'), res.get('kolID'))
    ress.close()
    print(f'========同步完成，共同步数据{i},失败{j}')


if __name__ == "__main__":
    for collection in collections:
        try:
            print(f'开始同步{collection}的数据')
            fromcollection = fromClient[fromdbname][collection]
            tocollection = toClient[todbname][collection]
            migrate(fromcollection, tocollection, )
            print(f'collention::: {collection} 同步完成')
        except Exception as e:
            print(f'{str(e)}')

    fromdbname = 'navigate-track'
    todbname = 'navigate-track'

    collections = [
        'postTrackSum'
    ]

    for collection in collections:
        try:
            print(f'开始同步{collection}的数据')
            fromcollection = fromClient[fromdbname][collection]
            tocollection = toClient[todbname][collection]
            migrate(fromcollection, tocollection, )
            print(f'collention::: {collection} 同步完成')
        except Exception as e:
            print(f'{str(e)}')

