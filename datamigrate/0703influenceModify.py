# --*-- coding: utf-8 _*_
# @Author   :liyingkun
# @time     :2020/7/3 2:47 下午
# @File     :0703influenceModify.py
# @Software :PyCharm

""" 修改influence的字段取值 为了大小比较方便 ，
同时需修改 model里的相应内容，fieldManager的相应部分
"""

import pymongo

fromMongoUrl = 'mongodb://192.168.1.27:27017'
toMongoUrl = 'mongodb://192.168.1.27:27017'
fromClient = pymongo.MongoClient(fromMongoUrl)
toClient = pymongo.MongoClient(toMongoUrl)

fromdbname = 'navigate-dev'
todbname = 'navigate-dev'

collections = [
    'mid_influencertwodminfo',
]


def migrate(fromcollection, tocollection):
    ress = fromcollection.find({}, {"_id": 1, "influence": 1}, no_cursor_timeout=True)
    i = 0
    j = 0
    for res in ress:
        # 映射
        DICT = {"A": 90, "B": 80, "C": 70, "D": 60, "S": 99}
        res['influence'] = DICT.get(res.get('influence'), 60)
        print(res)
        try:
            i = i + 1
            tocollection.update(
                {'_id': res.get('_id')},
                {'$set': res},
                True)
        except Exception as e:
            j = j + 1
            print(res.get('_id'), res.get('kolID'))
    ress.close()
    print(f'========同步完成，共同步数据{i},失败{j}')


for collection in collections:
    try:
        print(f'开始同步{collection}的数据')
        fromcollection = fromClient[fromdbname][collection]
        tocollection = toClient[todbname][collection]
        migrate(fromcollection, tocollection)
        print(f'collention::: {collection} 同步完成')
    except Exception as e:
        print(f'str(e')
