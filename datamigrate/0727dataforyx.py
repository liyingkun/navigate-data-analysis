# --*-- coding: utf-8 _*_
# @Author   :liyingkun
# @time     :2020/7/27 3:55 下午
# @File     :0727dataforyx.py
# @Software :PyCharm

""" 给运营提取数据 """

import pymongo

fromMongoUrl = 'mongodb://192.168.1.27:27017'
fromClient = pymongo.MongoClient(fromMongoUrl)

fromdbname = 'navigate-dev'

data = fromClient[fromdbname]['mid_influencertwodminfo'].find({}, {"_id": 0, "kolID": 1, "fans": 1, "platform": 1,
                                                                   "updateAtUtc": 1})

import pandas as pd
import numpy as np

df = pd.DataFrame(list(data))

df.head(5)
df = df.dropna()

df['month'] = df.updateAtUtc.apply(lambda x: f'{x.month}月')

df['fans_cut'] = pd.cut(df.fans, [0, 1000, 10000, 100000, 500000, 1000000, 2000000, np.inf],
                        labels=['A:0-1k', 'B:1k-1w', 'C:1w-10w', 'D:10w-50w', 'E:50w-100w', 'F:100w-200w', 'J:>200w'])


from dfply import *

stat = df >> group_by(X.platform, X.month, X.fans_cut) >> summarize(cnt=n(X.kolID))
ls = []
for i in set(df.platform.values):
    y = stat >> mask(X.platform == f'{i}') >> group_by(X.fans_cut) >> arrange(X.fans_cut, X.month) >> mutate(
        cumcnt=cumsum(X.cnt))

    y = y.sort_values(by=['fans_cut', 'month'])
    ls.append(y)

zstat = pd.concat(ls)

MapDict = {'A:0-1k': "观众", 'B:1k-1w': "素人", 'C:1w-10w': "素人", 'D:10w-50w': "尾部", 'E:50w-100w': "腰部",
           'F:100w-200w': "头部",
           'J:>200w': "头部"}
zstat['fans_label'] = zstat['fans_cut'].apply(lambda x: MapDict.get(x))

zstat.to_excel('kolStat_2020803.xlsx', index=False)
