# --*-- coding: utf-8 _*_
# @Author   :liyingkun
# @time     :2020/5/11 9:42 下午
# @File     :0511youtubeInput.py
# @Software :PyCharm
import json

import pandas as pd
import redis
import requests

df = pd.read_csv(r'/Users/yingkunli/Desktop/navigate-data-analysis/seedData/youtubeseed.csv')

channelIds = df.channelId.tolist()
llidx = range(0, 1050, 50)
# list(llidx)
ls = []
for i in range(len(llidx) - 1):
    _ = (channelIds[llidx[i]:llidx[i + 1]])
    ls.append(",".join(_))

channelIdLs = ls

# ==================
REDIS_HOST = 'redis-18317.c1.ap-southeast-1-1.ec2.cloud.redislabs.com'
REDIS_PORT = 18317
REDIS_DB = 0
REDIS_PASSWORD = 'RHUPogPkUT5atyOFef9lau79yB0jXxER'

r = redis.Redis(host=REDIS_HOST, port=REDIS_PORT, password=REDIS_PASSWORD)


def insertuid(uidList, redis_key):
    """
    将待爬取的任务插入到Redis中
    :param uidList:
    :return:
    """
    databasekey = redis_key.split(':')[0]
    for key in r.scan_iter(databasekey + '*'):
        r.delete(key)

    for uid in uidList:
        r.lpush(redis_key, uid)

    print('redis初始化完毕,{}'.format(redis_key))


insertuid(channelIdLs, "youtubeuser:url")
# insertuid(channelIds[-1:], "youtubeuser:url")

#########3


"""
def f_compute(platformUID):
    ETLserver = "http://127.0.0.1:7777/modelApi/v2/youtube"

    payload = {
        "type": "offline",
        "data": [
            {
                "channelId": platformUID
            }
        ]
    }
    try:
        payload = json.dumps(payload, ensure_ascii=False)
        res = requests.request("POST", ETLserver, data=payload.encode('utf-8'))
        print(res)
    except Exception as e:
        print(f'{str(e)},"---",{platformUID}')


import pymongo
import requests

fromMongoUrl = 'mongodb+srv://navigate-prod-user:KNkt0rq70dlNbSHc@navigate-prod.cktro.mongodb.net/?retryWrites=true&w=majority'
fromClient = pymongo.MongoClient(fromMongoUrl)
fromdbname = 'navigate-spider'

ress = fromClient[fromdbname]['youtube_videos'].find({}, {'_id': 0, "channelId": 1}, no_cursor_timeout=True)

reslist = list(ress)
for res in reslist:
    f_compute(res.get('channelId'))

"""
