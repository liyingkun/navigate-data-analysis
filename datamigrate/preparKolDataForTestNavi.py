# --*-- coding: utf-8 _*_
# @Author   :liyingkun
# @time     :2020/9/2 7:56 下午
# @File     :preparKolDataForTestNavi.py
# @Software :PyCharm

"""  迁徙数据 为了navigate1 """

""" 数据要求
1.帖子存在
2.area正确
3.平台尽可能覆盖，但不能太多

"""

import pymongo

# fromMongoUrl = 'mongodb://2o5572137z.qicp.vip:48547'
fromMongoUrl = 'mongodb://192.168.1.27:27017'
toMongoUrl = 'mongodb://192.168.1.27:27017'
fromClient = pymongo.MongoClient(fromMongoUrl)
toClient = pymongo.MongoClient(toMongoUrl)

fromdbname = 'navigate-dev'
todbname = 'navigate-test'

# 1.查询出有帖子的kol
kolIDs = fromClient[fromdbname]['mid_influencerpost'].distinct("kolID", {"NaviPostScore": {"$exists": True},
                                                                         "fans": {"$gte": 10000},
                                                                         "platform": {"$ne": "tiktok"}})

# 2.查询出tiktok能加载出图片的kol
kolIDsTik = fromClient[fromdbname]['mid_influencerpost'].distinct("kolID", {"NaviPostScore": {"$exists": True},
                                                                            "fans": {"$gte": 10000},
                                                                            "image": {"$regex": "navigate"},
                                                                            "platform": "tiktok"})

kolIDs = set(kolIDs) | set(kolIDsTik)
# 3.查询出有联系方式的kol
kolIDs2 = fromClient[fromdbname]['mid_influencertwodminfo'].distinct("kolID",
                                                                     {"isContact": True, "works": {"$exists": True},
                                                                      "fans": {"$gte": 10000},
                                                                      "platform": {"$ne": "instagram"}})

# 4.查询出Instagram头像能显示的

kolIDs2Ins = fromClient[fromdbname]['mid_influencertwodminfo'].distinct("kolID",
                                                                        {"isContact": True, "works": {"$exists": True},
                                                                         "fans": {"$gte": 10000},
                                                                         "avatar": {"$regex": "navigate"},
                                                                         "platform": "instagram"})

# 查询出微视和youtube
kolIDs2yw = fromClient[fromdbname]['mid_influencertwodminfo'].distinct("kolID",
                                                                       {"works": {"$exists": True},
                                                                        "fans": {"$gte": 10000},
                                                                        "platform": {"$in": ["weishi", "youtube"]}})
kolIDs2 = set(kolIDs2) | set(kolIDs2Ins) | set(kolIDs2yw)

# 5.求出交集,即为最终的满足条件的KOLID 集合

KOLID = set(kolIDs) & set(kolIDs2)

# 6.数据探索

ls = []
for kolID in KOLID:
    data = fromClient[fromdbname]['mid_influencertwodminfo'].find_one({"kolID": kolID},
                                                                      {"_id": 0, "kolID": 1, "platform": 1, "fans": 1,
                                                                       "area": 1,
                                                                       "industryType": 1, "works": 1})
    if data:
        ls.append(data)

import pandas as pd

df = pd.DataFrame(ls)

df.platform.value_counts()
df.industryType.value_counts()
df.area.value_counts()

""":info
tiktok         34345
douyin          8589
instagram       4314
xiaohongshu     4308
youtube         2201
bilibili        1941
weibo           1769
weishi             2

# weishi数据需要重爬

beauty           49590
entertainment     1667
other             1267
tech               729
fitness            699
fashion            608
anime              598
travel             425
gaming             376
pet                369
baby               225
lifestyle          206
gadget             161
education          158
food               114
automotive          83
sports              63
vehicle             38

中国       12337
中国·北京     1604
中国·广东      423
美国         356
         ...  
马达加斯加        1
关岛           1
黑山           1
萨尔瓦多         1
#128
"""

# 7.数据迁徙,kolinfo

for kolID in KOLID:
    data = fromClient[fromdbname]['mid_influencertwodminfo'].find_one({"kolID": kolID})
    if '·' in data.get("area"):
        data["area"] = "中国"
        data["areaCode"] = "CN"
    toClient[todbname]['mid_influencertwodminfo'].update_one({"kolID": kolID}, {"$set": data}, True)

# 8.数据迁徙，postinfo
for kolID in KOLID:
    data = fromClient[fromdbname]['mid_influencerpost'].find({"kolID": kolID})
    for post in data:
        print(post)
        toClient[todbname]['mid_influencerpost'].update_one({"idPost": post.get("idPost")}, {"$set": post}, True)

from concurrent import futures


def query_update(kolID):
    data = fromClient[fromdbname]['mid_influencerpost'].find({"kolID": kolID})
    for post in data:
        print(post)
        toClient[todbname]['mid_influencerpost'].update_one({"idPost": post.get("idPost")}, {"$set": post}, True)


def main():
    with futures.ThreadPoolExecutor() as executor:
        for future in executor.map(query_update, KOLID):
            pass


# main entrance
if __name__ == '__main__':
    main()
