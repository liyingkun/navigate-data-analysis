# --*-- coding: utf-8 _*_
# @Author   :liyingkun
# @time     :2020/9/2 9:38 下午
# @File     :ioTools.py
# @Software :PyCharm

""" 使用
    多线程
    asyncio+uvloop
的方式迁徙数据 提高效率
参考
https://www.jianshu.com/p/cac56b3d9a18
"""

from concurrent import futures


def query_update(kolID):
    data = fromClient[fromdbname]['mid_influencerpost'].find({"kolID": kolID})
    for post in data:
        print(post)
        toClient[todbname]['mid_influencerpost'].update_one({"idPost": post.get("idPost")}, {"$set": post}, True)


def main():
    with futures.ThreadPoolExecutor() as executor:
        for future in executor.map(query_update, KOLID):
            pass


# main entrance
if __name__ == '__main__':
    main()
