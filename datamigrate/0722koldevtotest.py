# --*-- coding: utf-8 _*_
# @Author   :liyingkun
# @time     :2020/7/22 3:29 下午
# @File     :0722postdevtotest.py
# @Software :PyCharm


import pymongo

fromMongoUrl = 'mongodb://root:dnqW9MriFu5P2x@120.76.101.229:27017/?authSource=admin'
toMongoUrl = 'mongodb://root:dnqW9MriFu5P2x@120.76.101.229:27017/?authSource=admin'
fromClient = pymongo.MongoClient(fromMongoUrl)
toClient = pymongo.MongoClient(toMongoUrl)

fromdbname = 'navigate-dev'
todbname = 'navigate-test'

collections = [
    # 'mid_influencerhotpost',
    # 'mid_influencertwodminfo',
    # 'mid_influencerstatistics',
    # 'mid_similarinfluencer',
    # 'mid_influencerpost',
    'influencers'
]


def migrate(fromcollection, tocollection):
    ress = fromcollection.find(no_cursor_timeout=True)
    i = 0
    j = 0
    for res in ress:
        try:
            i = i + 1
            tocollection.update(
                {'_id': res.get('_id')},
                {'$set': res},
                True)
        except Exception as e:
            j = j + 1
            print(res.get('_id'), res.get('kolID'))
    ress.close()
    print(f'========同步完成，共同步数据{i},失败{j}')


for collection in collections:
    try:
        print(f'开始同步{collection}的数据')
        fromcollection = fromClient[fromdbname][collection]
        tocollection = toClient[todbname][collection]
        migrate(fromcollection, tocollection)
        print(f'collention::: {collection} 同步完成')
    except Exception as e:
        print(f'str(e')
