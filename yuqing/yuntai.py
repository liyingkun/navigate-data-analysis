# --*-- coding: utf-8 _*_
# @Author   :liyingkun
# @time     :2020/8/17 5:37 下午
# @File     :yuntai.py
# @Software :PyCharm

""" 云台 hohem zhiyun dji 对比分析"""

import pandas as pd

dji = pd.read_csv(r'/Users/yingkunli/Desktop/navigate-data-analysis/yuqing/data/day_count_dji.csv')
dji['type'] = 'dji'
hohem = pd.read_csv(r'/Users/yingkunli/Desktop/navigate-data-analysis/yuqing/data/day_count_hohem.csv')
hohem['type'] = 'hohem'
zhiyun = pd.read_csv(r'/Users/yingkunli/Desktop/navigate-data-analysis/yuqing/data/day_count_zhiyun.csv')
zhiyun['type'] = 'zhiyun'

dfid = pd.concat([dji[['idPost', 'type']], hohem[['idPost', 'type']], zhiyun[['idPost', 'type']]])

import pymongo

fromMongoUrl = 'mongodb://192.168.1.27:27017'
client = pymongo.MongoClient(fromMongoUrl)
db = 'navigate-dev'
collection = 'mid_influencerpost'

dbpost = client[db][collection]

ids = dfid.idPost.values.tolist()
types = dfid.type.values.tolist()

data_list = []
for idpost, typ in zip(ids, types):
    data = dbpost.find_one({"idPost": idpost}, {"_id": 0, "images": 0, "seoMeta": 0, "authorInfos": 0,
                                                "authorStats": 0, "musicInfos": 0})
    data['fans'] = data.get('userInfo', {}).get('fans')
    data['displayName'] = data.get('userInfo', {}).get('displayName')
    data['type'] = typ
    data.pop('userInfo')
    data_list.append(data)

print(data_list)
import pandas as pd

cols = ['idPost', 'NaviPostScore', 'attitudes', 'collects', 'comments',
        'created_at', 'description', 'exposure', 'fans', 'image',
        'interactionRate', 'keywords', 'kolID', 'link', 'type',
        'participationRate', 'platform', 'platformUID', 'reposts', 'title',
        'views', 'workstype', 'displayName', 'dislikes']
df = pd.DataFrame(data_list)[cols]
df['date'] = df.created_at.apply(lambda x: x[0:10])

# 搜索的关键字匹配


# df.to_excel('./yuntai_raw_data.xlsx', index=False)

# 统计出最优的帖子
# 推广者粉丝最多
# 推广曝光最多
# 推广评分最高

from dfply import *

sel_cols = ['idPost', 'type', 'platform', 'displayName', 'link', 'NaviPostScore', 'reposts', 'attitudes', 'collects',
            'comments',
            'views', 'exposure', 'created_at', 'fans', 'interactionRate', 'participationRate',
            'workstype']
df = df[df.date.str.contains('2020')]
df = df.query('fans>0')
df_rank = df[sel_cols] >> group_by(X.platform, X.type) >> mutate(fans_drank=dense_rank(X.fans, ascending=False),
                                                                 exposure_drank=dense_rank(X.exposure, ascending=False),
                                                                 NaviPostScore_drank=dense_rank(X.NaviPostScore,
                                                                                                ascending=False))

df_rank = df_rank[df_rank.platform.isin(['tiktok', 'youtube', 'instagram'])]

max_min_scaler = lambda x: 1 - ((x - np.min(x)) / (np.max(x) - np.min(x)))
df_rank['fans_score'] = df_rank[['fans_drank']].apply(max_min_scaler)
df_rank['exposure_score'] = df_rank[['exposure_drank']].apply(max_min_scaler)
df_rank['navi_score'] = df_rank[['NaviPostScore_drank']].apply(max_min_scaler)

df_rank = df_rank >> mutate(rank_score=X.fans_score/2 + X.exposure_score + X.navi_score / 3)

df_rank_z = df_rank >> group_by(X.platform, X.type) >> \
            arrange(X.rank_score, ascending=False) >> head(3) >> ungroup()

df_rank_z = df_rank_z >> group_by(X.platform, X.type) >> mutate(rank=dense_rank(X.rank_score, ascending=False))

df_rank_z[sel_cols + ['rank']].to_excel('./yuntai_raw_data_rank.xlsx', index=False)
