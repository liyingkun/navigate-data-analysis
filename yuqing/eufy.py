# --*-- coding: utf-8 _*_
# @Author   :liyingkun
# @time     :2020/7/9 2:49 下午
# @File     :eufy.py
# @Software :PyCharm

""" eufy 分析"""

import pymongo

from yuqing.enfy_ids import ids

fromMongoUrl = 'mongodb://192.168.1.27:27017'
client = pymongo.MongoClient(fromMongoUrl)
db = 'navigate-dev'
collection = 'mid_influencerpost'

dbpost = client[db][collection]

data_list = []
for idpost in ids:
    data = dbpost.find_one({"idPost": idpost}, {"_id": 0, "images": 0, "seoMeta": 0, "authorInfos": 0,
                                                "authorStats": 0, "musicInfos": 0})
    data['fans'] = data.get('userInfo', {}).get('fans')
    data['displayName'] = data.get('userInfo', {}).get('displayName')
    data.pop('userInfo')
    data_list.append(data)

print(data_list)
import pandas as pd

cols = ['idPost', 'NaviPostScore', 'attitudes', 'collects', 'comments',
        'created_at', 'description', 'exposure', 'fans', 'image',
        'interactionRate', 'keywords', 'kolID', 'link',
        'participationRate', 'platform', 'platformUID', 'reposts', 'title',
        'views', 'workstype', 'displayName', 'dislikes']
df = pd.DataFrame(data_list)[cols]
df['date'] = df.created_at.apply(lambda x: x[0:10])

# df.to_excel('./eufy_raw_data.xlsx', index=False)
pass
