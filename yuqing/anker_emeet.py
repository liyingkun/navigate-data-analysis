# --*-- coding: utf-8 _*_
# @Author   :liyingkun
# @time     :2020/8/31 4:01 下午
# @File     :anker_emeet.py
# @Software :PyCharm

""" anker emeet 数据转换与尝试数据渲染 """

import json
import pandas as pd

with open('/Users/yingkunli/Desktop/navigate-data-analysis/yuqing/data/anker_emeet.json', 'r+') as f:
    jsondata = json.load(f)

vls = []
for item in jsondata.get('report').get('kolDetails'):
    for k, vs in item.items():
        for v in vs:
            v["brandName"] = k
            v["displayName"] = v.get('userInfo').get('displayName')
            vls.append(v)

pd.DataFrame(vls).to_excel("/Users/yingkunli/Desktop/navigate-data-analysis/yuqing/zuijia_anker_emeet.xlsx", index=False)

jsondata.get('report').get('fanSensitive')
v2ls = []
for k, vs in jsondata.get('report').get('fanSensitive').items():
    for v in vs:
        v["brandName"] = k
        v2ls.append(v)
pd.DataFrame(v2ls).to_csv("/Users/yingkunli/Desktop/navigate-data-analysis/yuqing/sentive_anker_emeet.csv", index=False)
