# --*-- coding: utf-8 _*_
# @Author   :liyingkun
# @time     :2020/7/9 5:24 下午
# @File     :wordcount.py
# @Software :PyCharm


""" 分词 """

import jieba
import pandas as pd

import pymongo

from yuqing.enfy_ids import ids

fromMongoUrl = 'mongodb://192.168.1.27:27017'
client = pymongo.MongoClient(fromMongoUrl)
db = 'navigate-dev'
collection = 'mid_influencerpost'

dbpost = client[db][collection]

data_list = []
for idpost in ids:
    data = dbpost.find_one({"idPost": idpost}, {"_id": 0, "description": 1, "platform": 1})
    data_list.append(data)

print(data_list)
import pandas as pd

df = pd.DataFrame(data_list)

df_ = df[df.platform.isin(['weibo', 'xiaohongshu', 'bilibili'])]

text = ",".join(df_.description.values.tolist())

" ".join(jieba.cut(text))
import jieba.analyse

tags = jieba.analyse.extract_tags(text, topK=100, withWeight=True)
tags = jieba.analyse.textrank(text, topK=100, withWeight=True)

print(",".join(tags))

df__ = df[df.platform.isin(['tiktok', 'youtube'])]
text = ",".join(df__.description.values.tolist())
tags = jieba.analyse.extract_tags(text, topK=100)
print(",".join(tags))
