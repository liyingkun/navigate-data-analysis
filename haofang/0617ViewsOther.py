# --*-- coding: utf-8 _*_
# @Author   :liyingkun
# @time     :2020/6/17 3:28 下午
# @File     :0617Views.py
# @Software :PyCharm

""" 浩方短链点击量需求
winnie.l@gadgetslootbox.com

{"idUser":ObjectId('5eddeb9d2029b600123716e3')}
{"idUser":ObjectId('5eb90bb7a541c90013aa19ef')}


"""

import pymongo
from bson.objectid import ObjectId

MongoUrl = 'mongodb+srv://navigate-prod-user:KNkt0rq70dlNbSHc@navigate-prod.cktro.mongodb.net/?retryWrites=true&w=majority'
client = pymongo.MongoClient(MongoUrl)

data = client['navigate-prod']['campaigns'].find({"idUser": ObjectId("5eb90bb7a541c90013aa19ef")},
                                                 {"_id": 1, "createdAt": 1, 'refLink': 1, 'name': 1})

dataList = list(data)

# 活动下面的tasks
Allres = []
for item in dataList:
    data = client['navigate-prod']['tasks'].find(
        {"idCampaign": item.get('_id')},
        {"shortLink": 1, "platform": 1, "totalClick": 1, "uniqueViews": 1}
    )

    _ = list(data)
    for d in _:
        Allres.append(dict(item, **d))

print(Allres)

import pandas as pd

df = pd.DataFrame(Allres)

df.to_excel('./haofangCampaign.xlsx', index=False)
