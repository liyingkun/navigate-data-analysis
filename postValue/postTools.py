# --*-- coding: utf-8 _*_
# @Author   :liyingkun
# @time     :2020/6/30 4:37 下午
# @File     :postTools.py
# @Software :PyCharm

""" 计算帖子相关指标
interactionRate
participationRate
fansRate
score

NaviPostScore
"""
import numpy as np
from scipy.stats import laplace
from typing import Dict


def f_interactionRate(attitudes, comments, fans):
    # 互动率 评论 点赞之和除以修正的粉丝数
    res = round((attitudes + comments) / (fans + 1), 4)
    if res > 1:
        return 1
    return res


def f_participationRate(attitudes, comments, views, reposts, collects, fans):
    """参与度 曝光量/粉丝数 """
    res = (attitudes + comments + (views / 1000) + reposts + collects) / (fans + 1)
    if res > 1:
        return 1
    return round(res, 4)


def f_fansScore(fans, w=0.2):
    return round(np.log10(fans + 1) / np.log10(1000000000) * w, 4)


def f_score(interactionRate, participationRate, fansScore, w1=0.5, w2=0.3):
    return interactionRate * w1 + participationRate * w2 + fansScore


def f_NaviPostScore(x):
    """ 依据score计算修正后 score """
    # res = laplace(0.13615992350595657, 0.021858929575996233).cdf(x)
    # 向上取整,最低值修正
    res = laplace(0.14, 0.03).cdf(x)-0.0047
    if res < 0:
        return 0
    if res > 1:
        return 100
    return round(res * 100, 2)


def f_NaviPostScoreKarg(data: Dict) -> float:
    """ 供单条数据使用 """
    if set(['attitudes', 'comments', 'views', 'reposts', 'collects', 'fans']).issubset(set(data.keys())):
        interactionRate = f_interactionRate(data['attitudes'], data['comments'], data['fans'])
        participationRate = f_participationRate(data.get('attitudes'), data.get('comments'), data['views'],
                                                data['reposts'],
                                                data['collects'], data.get('fans'))
        fansScore = f_fansScore(data['fans'])
        score = f_score(interactionRate, participationRate, fansScore)
        NaviPostScore = f_NaviPostScore(score)
        return NaviPostScore
    else:
        raise KeyError('传入数据中Key值不对')


if __name__ == "__main__":
    # from scipy.stats import genlogistic, laplace

    # laplace(0.14, 0.03).cdf(0.2)
    data = {'attitudes': 0, 'comments': 0, 'views': 0, 'reposts': 0, 'collects': 0,
            'fans': 1}
    NaviPostScoreKarg = f_NaviPostScoreKarg(data)
    print(NaviPostScoreKarg)
