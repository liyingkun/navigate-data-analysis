# --*-- coding: utf-8 _*_
# @Author   :liyingkun
# @time     :2020/6/30 1:27 下午
# @File     :demo.py
# @Software :PyCharm

import pymongo
import pandas as pd

# 1.查询出计算数据
# 2.补齐fans字段
# 3.计算相应的指标
# 4.写回数据

fromMongoUrl = 'mongodb://192.168.1.27:27017'
fromClient = pymongo.MongoClient(fromMongoUrl)

fromdbname = 'navigate-dev'

collections = 'mid_influencerpost'

projection = {"_id": 0, "idPost": 1, "kolID": 1, "collects": 1, "comments": 1,
              "reposts": 1, "views": 1, "attitudes": 1}

# 1
postdata = fromClient[fromdbname][collections].find({"workstype": {"$exists": True}}, projection)

# 2
fansdata = fromClient[fromdbname]['mid_influencertwodminfo'].find({}, {"_id": 0, "kolID": 1, "fans": 1})

# 3

postdf = pd.DataFrame(list(postdata))
fansdf = pd.DataFrame(list(fansdata))

df = pd.merge(fansdf, postdf, on='kolID', how='right')

# 4
from dfply import mutate, X, select
import numpy as np

statdf = df >> mutate(interactionRate=np.round((X.attitudes + X.comments) / (X.fans + 1), 4),
                      participationRate=np.round(
                          (X.attitudes + X.comments + (X.views / 1000) + X.reposts + X.collects) / (X.fans + 1), 4)
                      )

# 5
scoredf = statdf >> mutate(
    # score1=(X.fans + 1)/1000*0.2,
    score2=X.interactionRate * 0.5,
    score3=X.participationRate * 0.3)


def score1(x):
    return np.round(np.log10(x + 1) / np.log10(1000000000) * 0.2, 4)


scoredf['score1'] = scoredf.fans.apply(score1)

# 6.过滤掉异常值
scoredf = scoredf.query("score1>=0 and score2<=1 and score3<=1")

scoredf[['score1', 'score2', 'score3']].describe()

# 0701发现，前一天的数值，受粉丝的影响很大，加和之前，先进行标准化处理


scoredfZ = scoredf >> mutate(score=X.score1 + X.score2 + X.score3)

scoredfZ.score.describe()
"""
# 7.拟合分布

from fitter import Fitter

f = Fitter(scoredfZ.score)
f.distributions = ['genlogistic', "logistic"]
f.fit()
# f.get_best()
# f.summary()
x = f.get_best(method='sumsquare_error')  # 返回最佳拟合分布及其参数
# f.hist()  # 绘制组数=bins的标准化直方图
# f.plot_pdf(names=None, Nbest=3, lw=2)

{'laplace': (0.13615992350595657, 0.021858929575996233)}
{'genlogistic': (0.2974483648014779, 1.65004262136216, 0.112364225734054)}
"""


from scipy.stats import genlogistic, laplace

laplace(0.13615992350595657, 0.021858929575996233).cdf(0.2)


genlogistic(0.2974483648014779, 1.65004262136216, 0.112364225734054).cdf(1.1)

laplace.fit(scoredfZ.score)

def f_Npscore(x):
    """ 依据score计算修正后 score """
    return genlogistic(0.2974483648014779, 1.65004262136216, 0.112364225734054).cdf(x)


y = scoredfZ.score.apply(f_Npscore)
print(y)
y.describe()
