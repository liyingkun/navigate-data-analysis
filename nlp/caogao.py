# --*-- coding: utf-8 _*_
# @Author   :liyingkun
# @time     :2020/8/12 2:59 下午
# @File     :caogao.py
# @Software :PyCharm

text = '@20[1/2]【保险师】客户陈梦琳已通过您分享的链接购买了“妈咪保贝少儿重大疾病保险”，恭喜您获得推广费400.18元(不含奖励)，写评价赚宝石， '


def get_all_bxmc_sim(x):
    x = str(x)
    qindex = x.find('投保')
    if qindex == -1:
        qindex = x.find('购买')
        if qindex == -1:
            return None
        else:
            return x[qindex + 2:qindex + 17]
    else:
        return x[qindex + 2:qindex + 17]


get_all_bxmc_sim(text)

import pandas as pd

df = pd.read_excel(r'/Users/yingkunli/Desktop/navigate-data-analysis/nlp/stat_0812.xlsx')


# 车
def f_get_car(x):
    # 含有车的都提取出来
    if '车' in str(x):
        return 1
    else:
        return 0


df['car'] = df.xz.apply(f_get_car)

df_car = df.query("car==1")
# df_car.to_excel(r'/Users/yingkunli/Desktop/navigate-data-analysis/nlp/car_0812.xlsx', index=False)


# 健康医疗险

def f_get_yl(x):
    x = str(x)
    if '医' in x or '癌' in x or '一生' in x:
        return 1
    else:
        return 0


df['yl'] = df.xz.apply(f_get_yl)
df_yl = df.query("yl==1")
# df_yl.to_excel(r'/Users/yingkunli/Desktop/navigate-data-analysis/nlp/yl_0812.xlsx', index=False)

text = "投保重疾宝（福利版），保单号：60"


def get_all_bxmc(x):
    # 获取购买的保险名称
    x = str(x)
    qindex = x.find('投保')
    if qindex == -1:
        qindex = x.find('购买')
    zindex = x.find('险')
    diff = zindex - qindex
    if zindex == -1 or diff >= 10:
        zindex = x.find('宝')
        if zindex == -1:
            zindex = x.find('保')
    diff_ = zindex - qindex
    if qindex >= 0 and diff_ <= 10:
        return x[qindex + 2:zindex + 1]
    else:
        return None

get_all_bxmc(text)