# --*-- coding: utf-8 _*_
# @Author   :liyingkun
# @time     :2020/7/23 2:48 下午
# @File     :imagepipeline.py
# @Software :PyCharm

""" 图片下载pipeline """
import pymongo

import boto3
import requests
import os
from urllib.parse import urlparse

AWS_ACCESS_KEY_ID = "AKIAJTRVDMLLTVHIDZPA"
AWS_SECRET_KEY = "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"
AWS_REGION = "ap-southeast-1"
AWS_BUCKET_NAME = "navigate-network"


def upload_file_to_aws_s3(url='', file_type='image'):
    file_url = ''
    # get the connection of AWS S3 Bucket
    s3 = boto3.resource(
        's3',
        aws_access_key_id=AWS_ACCESS_KEY_ID,
        aws_secret_access_key=AWS_SECRET_KEY,
        region_name=AWS_REGION
    )

    response = requests.request("GET", str(url).replace('amp;', ''), headers={}, data={})
    if response.status_code == 200:
        raw_data = response.content
        url_parser = urlparse(url)
        file_name = os.path.basename("_".join(url_parser.path.split('/')))
        key = "/".join([file_type, file_name])

        try:
            # Write the raw data as byte in new file_name in the server
            with open(file_name, 'wb') as new_file:
                new_file.write(raw_data)

            # Open the server file as read mode and upload in AWS S3 Bucket.
            # data = open(file_name, 'rb')
            # s3.Bucket(AWS_BUCKET_NAME).put_object(Key=key, Body=data)
            # data.close()
            #
            s3.Bucket(AWS_BUCKET_NAME).upload_file(file_name, key, ExtraArgs={'ACL': 'public-read'})

            # Format the return URL of upload file in S3 Bucjet
            url_dict = {"AWS_BUCKET_NAME": AWS_BUCKET_NAME, "AWS_REGION": AWS_REGION, "key": key}
            file_url = "https://res.cloudinary.com/twodm/image/fetch/https://{AWS_BUCKET_NAME}.s3.{AWS_REGION}.amazonaws.com/{key}".format(
                **url_dict)
        except Exception as e:
            print("Error in file upload %s." % (str(e)))

        finally:
            # Close and remove file from Server
            new_file.close()
            os.remove(file_name)
            print("Attachment Successfully save in S3 Bucket url %s " % (file_url))
    else:
        print("Cannot parse url")
    return file_url


def get_imageS3url(info):
    if info.get('avatar') and AWS_BUCKET_NAME not in info.get('avatar'):
        s3url = upload_file_to_aws_s3(url=info.get('avatar'), file_type=info.get('platform', 'imgsrc'))
        info['avatar'] = s3url if s3url else info.get('avatar')
    if info.get('image') and AWS_BUCKET_NAME not in info.get('image'):
        s3url = upload_file_to_aws_s3(url=info.get('image'), file_type=info.get('platform', 'imgsrc'))
        info['image'] = s3url if s3url else info.get('image')

    return info


class ImgPipeline:
    """ 图片下载并上传至S3,并得到相应的URL,经研究，放到BaseModel中会比较好一点"""

    def __init__(self, local_mongo_host, local_mongo_port, mongo_db):
        self.local_mongo_host = local_mongo_host
        self.local_mongo_port = local_mongo_port
        self.mongo_db = mongo_db

    @classmethod
    def from_crawler(cls, crawler):
        return cls(
            local_mongo_host=crawler.settings.get('MONGO_HOST'),
            local_mongo_port=crawler.settings.get('MONGO_PORT'),
            mongo_db=crawler.settings.get('NAVIGATE_DB_NAME')
        )

    def open_spider(self, spider):
        self.client = pymongo.MongoClient(self.local_mongo_host, self.local_mongo_port)
        spider.logger.info(f'【{self.__class__.__name__}】connecting to mongo-->:{self.local_mongo_host} {self.mongo_db}')

        # 数据库名
        self.db = self.client[self.mongo_db]

    def close_spider(self, spider):
        self.client.close()

    def process_item(self, item, spider):
        pass


if __name__ == "__main__":
    # url = "https://p16-tiktok-va.ibyteimg.com/obj/tos-maliva-p-0068/66d148235ac037dd83ddca20bcf9ef26?x-expires=1595642400&x-signature=e5%2FKnxMdztvMq9%2B6QIEa140WevA%3D"
    # url = "http://wx4.sinaimg.cn/bmiddle/0075yGLyly1gb3gi6k75lj30u00u0n8b.jpg"
    # url = "https://scontent-sin6-2.cdninstagram.com/v/t51.2885-19/s150x150/92846766_2543822499202545_5899914119733051392_n.jpg?_nc_ht=scontent-sin6-2.cdninstagram.com&_nc_ohc=TihEDQrRfJcAX81Gigg&oh=4676167b6731f0fc03815821436e146b&oe=5EE3CFE5"
    # url = "https://p16-tiktokcdn-com.akamaized.net/aweme/100x100/tiktok-obj/1615193926916098.jpeg"
    # url = "https://scontent-hkg4-1.cdninstagram.com/v/t51.2885-19/s150x150/109229982_928432547637434_6447853425256498214_n.jpg?_nc_ht=scontent-hkg4-1.cdninstagram.com&_nc_ohc=bMse1MMacQcAX-YqzLt&oh=faf920c8f1e9adb09674ee919522ba94&oe=5F4229EC"
    # url = "https://p16-tiktok-va.ibyteimg.com/obj/tos-maliva-p-0068/36160bbb3fe840bebfe9467e63df3eba?x-expires=1595743200&x-signature=%2BONRuwvGEgyZVgzGCnnqkFDYwug%3D"
    url = "https://i0.hdslb.com/bfs/archive/1312022b4ad31bd6b48c85c66ea8c14ba813675c.jpg"
    url = "https://scontent-hkt1-1.cdninstagram.com/v/t51.2885-19/s150x150/75272185_535694083683954_7833278194735120384_n.jpg?_nc_ht=scontent-hkt1-1.cdninstagram.com&_nc_ohc=TDL7MdRKJZgAX94W7AO&oh=45051037d0c976a1c411f935a929c25f&oe=5F44E9BF"
    url = "http://i0.hdslb.com/bfs/archive/2ccff2a53d22806dd31fe19cef187676d37abef0.jpg"
    url = "https://p3-dy-ipv6.byteimg.com/aweme/100x100/2e33300016c1f3a7ac6be.jpeg?from=4010531038"
    file_type = "bilibili"
    upload_file_to_aws_s3(url, file_type)
