# --*-- coding: utf-8 _*_
# @Author   :liyingkun
# @time     :2020/9/3 9:35 上午
# @File     :dev2testkol.py
# @Software :PyCharm


import pymongo

# fromMongoUrl = 'mongodb://2o5572137z.qicp.vip:48547'

print("加载完毕")
fromMongoUrl = 'mongodb://192.168.1.27:27017'
toMongoUrl = 'mongodb://root:dnqW9MriFu5P2x@120.76.101.229:27017/?authSource=admin'
fromClient = pymongo.MongoClient(fromMongoUrl)
toClient = pymongo.MongoClient(toMongoUrl)

fromdbname = 'navigate-test'
todbname = 'navigate-test'

kolIDs = fromClient[fromdbname]['mid_influencertwodminfo'].find({}, {"kolID": 1, "_id": 0})


def query_kol_and_update(kolID):
    data = fromClient[fromdbname]['mid_influencertwodminfo'].find_one({"kolID": kolID.get("kolID")})
    toClient[todbname]['mid_influencertwodminfo'].update_one({"kolID": kolID}, {"$set": data}, True)


from concurrent import futures


def main_kol():
    print("开始红人迁徙")
    with futures.ThreadPoolExecutor(max_workers=100) as executor:
        executor.map(query_kol_and_update, list(kolIDs))


if __name__ == '__main__':
    import time

    st = time.time()
    main_kol()
    print(f"红人耗时:{time.time() - st}")
