# --*-- coding: utf-8 _*_
# @Author   :liyingkun
# @time     :2020/9/2 9:50 下午
# @File     :preparDesc.py
# @Software :PyCharm

""" 迁徙数据前的数据探索 """


# 6.数据探索

ls = []
for kolID in KOLID:
    data = fromClient[fromdbname]['mid_influencertwodminfo'].find_one({"kolID": kolID},
                                                                      {"_id": 0, "kolID": 1, "platform": 1, "fans": 1,
                                                                       "area": 1,
                                                                       "industryType": 1, "works": 1})
    if data:
        ls.append(data)

import pandas as pd

df = pd.DataFrame(ls)

df.platform.value_counts()
df.industryType.value_counts()
df.area.value_counts()

""":info
tiktok         34345
douyin          8589
instagram       4314
xiaohongshu     4308
youtube         2201
bilibili        1941
weibo           1769
weishi             2

# weishi数据需要重爬

beauty           49590
entertainment     1667
other             1267
tech               729
fitness            699
fashion            608
anime              598
travel             425
gaming             376
pet                369
baby               225
lifestyle          206
gadget             161
education          158
food               114
automotive          83
sports              63
vehicle             38

中国       12337
中国·北京     1604
中国·广东      423
美国         356
         ...  
马达加斯加        1
关岛           1
黑山           1
萨尔瓦多         1
#128
"""
