# --*-- coding: utf-8 _*_
# @Author   :liyingkun
# @time     :2020/9/2 7:56 下午
# @File     :preparKolDataForTestNavi.py
# @Software :PyCharm

"""  迁徙数据 为了navigate1 """

""" 数据要求
1.帖子存在
2.area正确
3.平台尽可能覆盖，但不能太多

"""

import pymongo

# fromMongoUrl = 'mongodb://2o5572137z.qicp.vip:48547'
fromMongoUrl = 'mongodb://192.168.1.27:27017'
toMongoUrl = 'mongodb://192.168.1.27:27017'
fromClient = pymongo.MongoClient(fromMongoUrl)
toClient = pymongo.MongoClient(toMongoUrl)

fromdbname = 'navigate-dev'
todbname = 'navigate-test'

# 1.查询出有帖子的kol
kolIDs = fromClient[fromdbname]['mid_influencerpost'].distinct("kolID", {"NaviPostScore": {"$exists": True},
                                                                         "fans": {"$gte": 10000},
                                                                         "platform": {"$ne": "tiktok"}})

# 2.查询出tiktok能加载出图片的kol
kolIDsTik = fromClient[fromdbname]['mid_influencerpost'].distinct("kolID", {"NaviPostScore": {"$exists": True},
                                                                            "fans": {"$gte": 10000},
                                                                            "image": {"$regex": "navigate"},
                                                                            "platform": "tiktok"})

kolIDs = set(kolIDs) | set(kolIDsTik)
# 3.查询出有联系方式的kol
kolIDs2 = fromClient[fromdbname]['mid_influencertwodminfo'].distinct("kolID",
                                                                     {"isContact": True, "works": {"$exists": True},
                                                                      "fans": {"$gte": 10000},
                                                                      "platform": {"$ne": "instagram"}})

# 4.查询出Instagram头像能显示的

kolIDs2Ins = fromClient[fromdbname]['mid_influencertwodminfo'].distinct("kolID",
                                                                        {"isContact": True, "works": {"$exists": True},
                                                                         "fans": {"$gte": 10000},
                                                                         "avatar": {"$regex": "navigate"},
                                                                         "platform": "instagram"})

# 查询出微视和youtube
kolIDs2yw = fromClient[fromdbname]['mid_influencertwodminfo'].distinct("kolID",
                                                                       {"works": {"$exists": True},
                                                                        "fans": {"$gte": 10000},
                                                                        "platform": {"$in": ["weishi", "youtube"]}})
kolIDs2 = set(kolIDs2) | set(kolIDs2Ins) | set(kolIDs2yw)

# 5.求出交集,即为最终的满足条件的KOLID 集合

KOLID = set(kolIDs) & set(kolIDs2)

# 6.数据探索 见desc脚本

# 7.数据迁徙,kolinfo

# 8.数据迁徙，postinfo
