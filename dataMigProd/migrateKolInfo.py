# --*-- coding: utf-8 _*_
# @Author   :liyingkun
# @time     :2020/9/2 9:51 下午
# @File     :migrateKolInfo.py
# @Software :PyCharm

""" 迁徙kol基本信息 """

# 7.数据迁徙,kolinfo
from dataMigProd.preparKolDataForTestNavi import KOLID, fromClient, fromdbname, toClient, todbname

for kolID in KOLID:
    data = fromClient[fromdbname]['mid_influencertwodminfo'].find_one({"kolID": kolID})
    if '·' in data.get("area"):
        data["area"] = "中国"
        data["areaCode"] = "CN"
    toClient[todbname]['mid_influencertwodminfo'].update_one({"kolID": kolID}, {"$set": data}, True)

