# --*-- coding: utf-8 _*_
# @Author   :liyingkun
# @time     :2020/9/7 4:09 下午
# @File     :imageUpdate.py
# @Software :PyCharm

# 头像和帖子封面图问题


import pymongo

# fromMongoUrl = 'mongodb://2o5572137z.qicp.vip:48547'
# from dataMigProd.preparKolDataForTestNavi import KOLID
from dataMigProd.imagepipeline import get_imageS3url

print("加载完毕")
fromMongoUrl = 'mongodb://192.168.1.27:27017'
toMongoUrl = 'mongodb://root:dnqW9MriFu5P2x@120.76.101.229:27017/?authSource=admin'
fromClient = pymongo.MongoClient(fromMongoUrl)
toClient = pymongo.MongoClient(toMongoUrl)

fromdbname = 'navigate-test'
todbname = 'navigate-test'

# 先弄哔哩哔哩
data = fromClient[fromdbname]["mid_influencerpost"].find(
    {"image": {"$regex": "^(?!.*navigate)"}, "platform": "bilibili"}, {"_id": 0})

from concurrent import futures


def update_bilibili_image(post):
    # 0907发现，此处需要注意条件
    info = get_imageS3url(post)
    fromClient[fromdbname].update_one({"idPost": post.get("idPost")}, {"$set": info}, True)


def main():
    with futures.ThreadPoolExecutor(max_workers=100) as executor:
        executor.map(update_bilibili_image, list(data))


# main entrance
if __name__ == '__main__':
    import time

    st = time.time()
    main()
    print(f"更新图片耗时:{time.time() - st}")
