# --*-- coding: utf-8 _*_
# @Author   :liyingkun
# @time     :2020/9/7 5:18 下午
# @File     :keywordsWeibug.py
# @Software :PyCharm

"""解决微视 keywords的bug """

import pymongo

# fromMongoUrl = 'mongodb://2o5572137z.qicp.vip:48547'
# from dataMigProd.preparKolDataForTestNavi import KOLID

print("加载完毕")
fromMongoUrl = 'mongodb://192.168.1.27:27017'
toMongoUrl = 'mongodb://root:dnqW9MriFu5P2x@120.76.101.229:27017/?authSource=admin'
fromClient = pymongo.MongoClient(fromMongoUrl)
toClient = pymongo.MongoClient(toMongoUrl)

fromdbname = 'navigate-test'
todbname = 'navigate-test'

data = fromClient[fromdbname]["mid_influencerpost"].find({"platform": "weishi"}, {"_id": 0})

for post in data:
    print("===", post.get("keywords"))
    post["keywords"] = []
    print("===", post["keywords"])
    fromClient[fromdbname]["mid_influencerpost"].update_one({"idPost": post.get("idPost")}, {"$set": post}, True)

data = toClient[todbname]["mid_influencerpost"].find({"platform": "weishi"}, {"_id": 0})
for post in data:
    print("===___", post.get("keywords"))
    post["keywords"] = []
    print("===___", post["keywords"])
    toClient[todbname]["mid_influencerpost"].update_one({"idPost": post.get("idPost")}, {"$set": post}, True)
