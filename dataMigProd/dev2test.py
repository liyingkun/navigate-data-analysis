# --*-- coding: utf-8 _*_
# @Author   :liyingkun
# @time     :2020/9/2 10:09 下午
# @File     :dev2test.py
# @Software :PyCharm

""" 从本地机房，迁徙到阿里云服务器 """

import pymongo

# fromMongoUrl = 'mongodb://2o5572137z.qicp.vip:48547'
from dataMigProd.preparKolDataForTestNavi import KOLID

print("加载完毕")
fromMongoUrl = 'mongodb://192.168.1.27:27017'
toMongoUrl = 'mongodb://root:dnqW9MriFu5P2x@120.76.101.229:27017/?authSource=admin'
fromClient = pymongo.MongoClient(fromMongoUrl)
toClient = pymongo.MongoClient(toMongoUrl)

fromdbname = 'navigate-test'
todbname = 'navigate-test'


# 迁徙 kolinfo


def query_kol_update(kolID):
    data = fromClient[fromdbname]['mid_influencertwodminfo'].find_one({"kolID": kolID})
    toClient[todbname]['mid_influencertwodminfo'].update_one({"kolID": kolID}, {"$set": data}, True)


from concurrent import futures


def main_kol():
    print("开始红人迁徙")
    with futures.ThreadPoolExecutor(max_workers=100) as executor:
        executor.map(query_kol_update, list(KOLID))


def query_post_update(kolID):
    data = fromClient[fromdbname]['mid_influencerpost'].find({"kolID": kolID})
    for post in data:
        # print(post)
        toClient[todbname]['mid_influencerpost'].update_one({"idPost": post.get("idPost")}, {"$set": post}, True)


def main_post():
    print("开始帖子迁徙")
    with futures.ThreadPoolExecutor(max_workers=100) as executor:
        executor.map(query_post_update, list(KOLID))


if __name__ == '__main__':
    import time

    st = time.time()
    # main_kol()
    print(f"红人耗时:{time.time() - st}")

    st = time.time()
    main_post()
    print(f"帖子耗时:{time.time() - st}")
