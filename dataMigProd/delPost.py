# --*-- coding: utf-8 _*_
# @Author   :liyingkun
# @time     :2020/9/7 3:21 下午
# @File     :delPost.py
# @Software :PyCharm

""" 删除不满足要求的帖子 """

import pymongo

# fromMongoUrl = 'mongodb://2o5572137z.qicp.vip:48547'
from dataMigProd.preparKolDataForTestNavi import KOLID

print("加载完毕")
fromMongoUrl = 'mongodb://192.168.1.27:27017'
toMongoUrl = 'mongodb://root:dnqW9MriFu5P2x@120.76.101.229:27017/?authSource=admin'
fromClient = pymongo.MongoClient(fromMongoUrl)
toClient = pymongo.MongoClient(toMongoUrl)

fromdbname = 'navigate-test'
todbname = 'navigate-test'

fromClient[fromdbname]["mid_influencerpost"].delete_many({"NaviPostScore": {"$exists": False}})
toClient[todbname]["mid_influencerpost"].delete_many({"NaviPostScore": {"$exists": False}})
