# --*-- coding: utf-8 _*_
# @Author   :liyingkun
# @time     :2020/9/3 9:07 上午
# @File     :dev2testPost.py
# @Software :PyCharm


import pymongo

# fromMongoUrl = 'mongodb://2o5572137z.qicp.vip:48547'

print("加载完毕")
fromMongoUrl = 'mongodb://192.168.1.27:27017'
toMongoUrl = 'mongodb://root:dnqW9MriFu5P2x@120.76.101.229:27017/?authSource=admin'
fromClient = pymongo.MongoClient(fromMongoUrl)
toClient = pymongo.MongoClient(toMongoUrl)

fromdbname = 'navigate-test'
todbname = 'navigate-test'

idPosts = fromClient[fromdbname]['mid_influencerpost'].find({}, {"idPost": 1, "_id": 0})


def query_post_and_update(idPost):
    data = fromClient[fromdbname]['mid_influencerpost'].find_one({"idPost": idPost.get("idPost")})
    print(data.get("idPost"))
    toClient[todbname]['mid_influencerpost'].update_one({"idPost": idPost}, {"$set": data}, True)


from concurrent import futures


def main_post():
    print("开始帖子迁徙")
    with futures.ThreadPoolExecutor(max_workers=100) as executor:
        executor.map(query_post_and_update, list(idPosts))


if __name__ == '__main__':
    import time

    st = time.time()
    main_post()
    print(f"帖子耗时:{time.time() - st}")
