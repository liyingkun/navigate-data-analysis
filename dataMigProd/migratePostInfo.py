# --*-- coding: utf-8 _*_
# @Author   :liyingkun
# @time     :2020/9/2 9:52 下午
# @File     :migratePostInfo.py
# @Software :PyCharm
# https://www.jianshu.com/p/cac56b3d9a18
""" 迁徙帖子相关 """

# 8.数据迁徙，postinfo
from dataMigProd.preparKolDataForTestNavi import KOLID, fromClient, fromdbname, toClient, todbname
#
# for kolID in KOLID:
#     data = fromClient[fromdbname]['mid_influencerpost'].find({"kolID": kolID})
#     for post in data:
#         print(post)
#         toClient[todbname]['mid_influencerpost'].update_one({"idPost": post.get("idPost")}, {"$set": post}, True)

from concurrent import futures


def query_update(kolID):
    # 0907发现，此处需要注意条件
    data = fromClient[fromdbname]['mid_influencerpost'].find({"kolID": kolID})
    for post in data:
        # print(post)
        # 判断post中的字段是否全 TODO 集中处理速度上更有优势
        toClient[todbname]['mid_influencerpost'].update_one({"idPost": post.get("idPost")}, {"$set": post}, True)


def main():
    with futures.ThreadPoolExecutor(max_workers=100) as executor:
        executor.map(query_update, list(KOLID)[1000:])


# main entrance
if __name__ == '__main__':
    import time

    st = time.time()
    main()
    print(f"耗时:{time.time() - st}")
