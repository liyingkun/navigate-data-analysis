# --*-- coding: utf-8 _*_
# @Author   :liyingkun
# @time     :2020/5/3 9:05 下午
# @File     :xiaohongshuInput.py
# @Software :PyCharm

"""
小红书爬取用户入库
"""

import pymongo

from esOne.models import SuperSearchUserInfo, XiaohongshuUserInfo
from esOne.testESmap import SuperSearchInfo

client = pymongo.MongoClient('mongodb://2o5572137z.qicp.vip:48547')
db = client['navigate-spider']

ress = db['XhsUser'].find({}, {'_id': 0})

i = 0
for xiaohongshu in ress:
    try:
        i += 1
        print(f'===={i}=====')
        res = XiaohongshuUserInfo.parse_obj(xiaohongshu.get('userDetail'))
        info = res.etl()
        userinfo = SuperSearchUserInfo(**info)
        data = userinfo.dict()
        result = SuperSearchInfo(meta={'id': data.get('kolID')}, **data)
        result.save()
    except Exception as e:
        print(f'{str(e)}')
        print(f'{xiaohongshu}')

# 58