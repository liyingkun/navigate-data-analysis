# --*-- coding: utf-8 _*_
# @Author   :liyingkun
# @time     :2020/5/3 8:25 下午
# @File     :douyinInput.py
# @Software :PyCharm

""" 抖抖侠数据入库 """

import pymongo

from esOne.models import SuperSearchUserInfo, DouyinUserInfo
from esOne.testESmap import SuperSearchInfo

client = pymongo.MongoClient('mongodb://2o5572137z.qicp.vip:48547')
db = client['navigate-spider']

ress = db['DdxUser'].find({}, {'_id': 0})

i = 0
for douyin in ress:
    try:
        i += 1
        print(f'===={i}=====')
        res = DouyinUserInfo.parse_obj(douyin.get('author'))
        info = res.etl()
        userinfo = SuperSearchUserInfo(**info)
        data = userinfo.dict()
        result = SuperSearchInfo(meta={'id': data.get('kolID')}, **data)
        result.save()
    except Exception as e:
        print(f'{str(e)}')
        print(f'{douyin}')
