# --*-- coding: utf-8 _*_
# @Author   :liyingkun
# @time     :2020/5/3 7:08 下午
# @File     :weiboInput.py
# @Software :PyCharm

""" 微博数据入ES代码demo """

import pymongo

from esOne.models import WeiboUserInfo, SuperSearchUserInfo
from esOne.testESmap import SuperSearchInfo

client = pymongo.MongoClient('mongodb://2o5572137z.qicp.vip:48547')
db = client['navigate-spider']

ress = db['weiboUser'].find({}, {'_id': 0})

"""
i = 0
for weibo in ress:
    try:
        i += 1
        print(f'===={i}=====')
        res = WeiboUserInfo.parse_obj(weibo)
        info = res.etl()
        userinfo = SuperSearchUserInfo(**info)
        data = userinfo.dict()
        result = SuperSearchInfo(meta={'id': data.get('kolID')}, **data)
        result.save()
    except Exception as e:
        print(f'{str(e)}')
        print(f'{weibo}')
"""
# 1824 [1823] 2359[2358] 2775[2774] 2912[2911]
ress = list(ress)
for i in [1823, 2358, 2774, 2911]:
    weibo = ress[i]
    res = WeiboUserInfo.parse_obj(weibo)
    info = res.etl()
    userinfo = SuperSearchUserInfo(**info)
    data = userinfo.dict()
    result = SuperSearchInfo(meta={'id': data.get('kolID')}, **data)
    result.save()
