# --*-- coding: utf-8 _*_
# @Author   :liyingkun
# @time     :2020/4/30 5:40 下午
# @File     :industryTool.py
# @Software :PyCharm
""" 中文平台 行业模型 """
import os
import pickle
import jieba

PROJECTPATH = os.path.abspath(os.path.dirname(os.path.dirname(__file__)))
MODELPATH = os.path.join(PROJECTPATH, 'industry/models')


def getCutword(x):
    seg_list = jieba.cut_for_search(str(x))  # 搜索引擎模式
    return " ".join(seg_list) + '\n'


def getCountVectorizer(vectoerizer, cutword):
    X = vectoerizer.transform(cutword)
    return X


def getTfidf(tfidf_transformer, cv):
    return tfidf_transformer.transform(cv)


def getKBest(ch2, X_test):
    return ch2.transform(X_test)


indcode = {'photography': 0,
           'anime': 1,
           'fashion': 2,
           'other': 3,
           'travel': 4,
           'entertainment': 5,
           'tech': 6,
           'gaming': 7,
           'beauty': 8,
           'wellbeing': 9,
           'education': 10,
           'pet': 11,
           'baby': 12,
           'sports': 13,
           'medical': 14,
           'gadget': 15,
           'fitness': 16,
           'automotive': 17,
           'news': 18,
           'home': 19}

codeind = {v: k for k, v in indcode.items()}


class PredictInductry(object):
    def __init__(self, industrydoc, featureModelPath, modelPath):
        self.industrydoc = industrydoc
        self.featureModelPath = featureModelPath
        self.modelPath = modelPath
        self._load_featureModel()
        self._load_model()
        self._extraFeature()

    def _load_featureModel(self):
        with open(self.featureModelPath, 'rb') as f:
            self.featureModel = pickle.load(f)
        self.cv = self.featureModel['cv']
        self.tfidf = self.featureModel['tfidf']
        self.ch2 = self.featureModel['ch2']

    def _load_model(self):
        with open(self.modelPath, 'rb') as f:
            self.model = pickle.load(f)

    def _extraFeature(self):
        cutword = [getCutword(self.industrydoc)]
        cv = getCountVectorizer(self.cv, cutword)
        tfidf = getTfidf(self.tfidf, cv)
        X_test = getKBest(self.ch2, tfidf.toarray())
        self.feature = X_test

    def _predict(self):
        return self.model.predict(self.feature)[0]

    def _predictIndustry(self):
        return codeind[self._predict()]


def getIndustry(doc):
    featureModelPath = os.path.join(MODELPATH, 'featureModel.pkl')
    modelPath = os.path.join(MODELPATH, 'industryModel.model')
    pit = PredictInductry(doc, featureModelPath, modelPath)
    return pit._predictIndustry()


if __name__ == "__main__":
    desc = ""
    x = getIndustry(desc)

    print(x)
