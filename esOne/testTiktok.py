# --*-- coding: utf-8 _*_
# @Author   :liyingkun
# @time     :2020/4/22 3:51 下午
# @File     :testTiktok.py
# @Software :PyCharm
from esOne.models import TiktokUserInfo, SuperSearchUserInfo

tiktok = {
    "secUid": "MS4wLjABAAAAEjxOaFdz-EXDYWI2Qz9X6uxMQeQIKaAzkAWyInZ6M309-FuHzhDzDAIyUaVRxLME",
    "userId": "6748953635118187521",
    "isSecret": False,
    "uniqueId": "cnliziqi",
    "nickName": "李子柒",
    "signature": "李家有女，人称子柒\nOfficial Account， welcome subscribe to my YT 李子柒liziqi",
    "covers": [
        "https://p16-tiktokcdn-com.akamaized.net/obj/tiktok-obj/1648254144697346"
    ],
    "coversMedium": [
        "https://p16-tiktokcdn-com.akamaized.net/obj/tiktok-obj/1648254144697346"
    ],
    "following": 0,
    "fans": 871816,
    "heart": "5094594",
    "video": 273,
    "verified": True,
    "digg": 1
}

if __name__ == "__main__":
    res = TiktokUserInfo(**tiktok)
    info = res.etl()
    userinfo = SuperSearchUserInfo(**info)
    print(userinfo.json(ensure_ascii=False))
