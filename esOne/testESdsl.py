# --*-- coding: utf-8 _*_
# @Author   :liyingkun
# @time     :2020/4/23 10:30 上午
# @File     :testESdsl.py
# @Software :PyCharm

"""
使用DSL使用

"""
from elasticsearch import Elasticsearch
from elasticsearch_dsl import Search

client = Elasticsearch(hosts=[{"host": "2dm-dnns.ticp.io", "port": 42836}])

s = Search(using=client)

res = s.query("multi_match", query='李子柒', fields=['displayName', 'desc'])
response = res.execute()
response.hits.total.value
response.took
print(response.hits.total.relation)
# print(response.suggest.my_suggestions)
for hit in response.hits:
    print(hit, hit.displayName, hit.desc, hit.platform)

dir(response)

hit.to_dict()

s = s.highlight('displayName')
# or, including parameters:
s = s.highlight('displayName', fragment_size=50)


# 搜索的同时 实现多层过滤
res.filter('terms', platform=["weibo", "tiktok"]).filter("terms", platform=["weibo"]).execute()
