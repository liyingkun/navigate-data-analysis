# ES 服务相关

- 拉取es
docker pull docker.elastic.co/elasticsearch/elasticsearch:7.6.2

- 启动
docker run  -p 9200:9200 -p 9300:9300 --name 2dmes -v /media/ssd/esdata:/usr/share/elasticsearch/data -e "discovery.type=single-node" docker.elastic.co/elasticsearch/elasticsearch:7.6.2
docker run -d -p 9200:9200 -p 9300:9300 -v /media/ssd/esdata:/usr/share/elasticsearch/data -e "discovery.type=single-node" docker.elastic.co/elasticsearch/elasticsearch:7.6.2

-拉取kibana
docker pull docker.elastic.co/kibana/kibana:7.6.2

docker run --link YOUR_ELASTICSEARCH_CONTAINER_NAME_OR_ID:elasticsearch -p 5601:5601 {docker-repo}:{version}
docker run -d --link 3e79c5badf60:elasticsearch -p 5601:5601 docker.elastic.co/kibana/kibana:7.6.2

- 网页端打开
es:  http://2dm-dnns.ticp.io:42836/
kibana:  http://2o5572137z.qicp.vip:12425/app/kibana#/dev_tools/console

pip install elasticsearch


-

docker run  -d --restart=on-failure:5  -p 9200:9200 -p 9300:9300 --name 2dmes -v /media/ssd/esdata:/usr/share/elasticsearch/data -e "discovery.type=single-node" docker.elastic.co/elasticsearch/elasticsearch:7.6.2
docker run -d --restart=on-failure:5  --link 2dmes:elasticsearch -p 5601:5601 docker.elastic.co/kibana/kibana:7.6.2

-
安装ik

elasticsearch-plugin install https://github.com/medcl/elasticsearch-analysis-ik/releases/download/v7.6.2/elasticsearch-analysis-ik-7.6.2.zip
elasticsearch-plugin install https://github.com/medcl/elasticsearch-analysis-pinyin/releases/download/v7.6.2/elasticsearch-analysis-pinyin-7.6.2.zip