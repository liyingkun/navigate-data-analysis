# --*-- coding: utf-8 _*_
# @Author   :liyingkun
# @time     :2020/4/21 5:34 下午
# @File     :models.py
# @Software :PyCharm
import pytz
from pydantic import BaseModel, validator, Field, root_validator
from typing import List, Dict
from enum import Enum
from datetime import datetime
from pydantic import AnyUrl, EmailStr

from esOne.areaTool import tansarea, getAreaCode
from esOne.contactTool import myextractor
from pypinyin import lazy_pinyin
import uuid

from industry.industryTool import getIndustry


def getKolID(platform, platformUID):
    """
    计算kolID
    计算方式为：platform + _ + platformUID uuid3 dns
    :param platform: 社交平台名称
    :param platformUID: 社交平台对用户的唯一标识
    :return:
    """
    temp = platform + '_' + str(platformUID)
    kolID = str(uuid.uuid3(uuid.NAMESPACE_DNS, temp))
    return kolID


class PlatformEnum(str, Enum):
    weibo = 'weibo'
    xiaohongshu = 'xiaohongshu'
    douyin = 'douyin'
    weishi = 'weishi'
    bilibili = 'bilibili'
    youtube = 'youtube'
    instagram = 'instagram'
    tiktok = 'tiktok'


class GenderEnum(str, Enum):
    female = 0
    male = 1
    other = 2
    miss = 3


class EmailEmptyAllowedStr(EmailStr):
    # 允许邮箱为空的处理 参考 https://github.com/samuelcolvin/pydantic/issues/181
    @classmethod
    def validate(cls, value: str) -> str:
        if value == "":
            return value
        return super().validate(value)


class SuperSearchUserInfo(BaseModel):
    """
    超级搜索1期 用户资料
    """

    kolID: str = ''
    platform: PlatformEnum
    platformUID: str
    displayName: str
    displayNamePy: str
    avatar: AnyUrl  # 加代理
    platformLink: AnyUrl
    desc: str = ""
    fans: int = 0
    works: int = 0
    follows: int = 0
    likes: int = 0
    collected: int = 0
    comments: int = 0
    views: int = 0
    exposuresAvg: int = 0
    isVerify: bool = False
    verifyInfo: str = ""
    gender: GenderEnum = GenderEnum.miss
    age: int = 0
    publishedAt: datetime = datetime(1970, 1, 1, 00, 00)
    areaCode: str = ''
    area: str = ''
    address: str = ''
    formatAddr: Dict = {}
    country: str = ''
    province: str = ''
    city: str = ''
    industries: List = []
    industryType: str = ''
    email: EmailEmptyAllowedStr = ""
    phone: str = ''
    otherContact: str = ''
    isContact: bool = False
    accountType: str = ''
    accountRank: str = ''
    updateAtUtc: datetime = datetime.utcnow()
    updateAtCst: datetime = datetime.now(tz=pytz.timezone('Asia/Shanghai'))

    # 头像加代理
    @validator('avatar', always=True)
    def avatar_have_proxy(cls, v):
        if 'https://res.cloudinary.com/twodm/image/fetch/' not in v:
            return ''.join(['https://res.cloudinary.com/twodm/image/fetch/', v])
        return v

    # 平均曝光的计算（曝光量除以作品数）
    @validator('exposuresAvg', always=True)
    def get_exposuresAvg(cls, v, values, **kwargs):
        col = ["fans", "follows", "likes", "collected", "comments", "views"]
        exposures = [values.get(i) for i in col]
        if values.get('works') == 0:
            return sum(exposures) / (values.get('works') + 1)
        return int(round(sum(exposures) / values.get('works'), 0))

    # 是否有联系方式
    @validator('isContact', always=True)
    def get_isContact(cls, v, values, **kwargs):
        if values.get('email') or values.get('phone') or values.get('otherContact'):
            return True
        return v

    # 地区的解析与映射
    @root_validator()
    def get_area(cls, values):
        values['area'] = tansarea(values.get('areaCode'))
        return values

        # 行业的解析与映射

    # 联系方式的解析与映射
    @root_validator(pre=True)
    def email_extract(cls, values):
        """v为校验的字段值 values为model的所有fields"""
        text = values.get('desc').strip()
        if not text:
            v = ''
            values['email'] = v
            return values
        mex = myextractor()
        # values['email'] = ", ".join(mex.extract_email(text))
        # 当有多个时，就不能通过验证
        emails = mex.extract_email(text)
        if emails:
            values['email'] = emails[0]
        else:
            values['email'] = ""
        return values

    @root_validator(pre=True)
    def phone_extract(cls, values):
        """v为校验的字段值 values为model的所有fields"""
        text = values.get('desc').strip()
        if not text:
            v = ''
            values['phone'] = v
            return values
        mex = myextractor()
        values['phone'] = ", ".join(mex.extract_cellphone(text, nation='CHN'))
        return values

    @root_validator(pre=True)
    def otherContact_extract(cls, values):
        """v为校验的字段值 values为model的所有fields"""
        text = values.get('desc').strip()
        if not text:
            v = ''
            values['otherContact'] = v
            return values
        if text and values.get('platform') not in ['youtube', 'instagram', 'tiktok']:
            mex = myextractor()
            qq = ", ".join(mex.extract_qq(text))
            wechat = ", ".join(mex.extract_wechat(text))
            values['otherContact'] = "qq/wechat:{}{}".format(qq, wechat) if qq or wechat else ""
            return values
        return values

    @root_validator(pre=True)
    def get_displayNamePy(cls, values):
        """v为校验的字段值 values为model的所有fields"""
        text = values.get('displayName').strip()
        res = "".join(lazy_pinyin(text))
        values['displayNamePy'] = res
        return values

    @root_validator(pre=True)
    def get_kolID(cls, values):
        values['kolID'] = getKolID(values.get('platform'), values.get('platformUID'))
        return values


class WeiboUserInfo(BaseModel):
    """
    爬取到的微博数据-用户基本信息
    {
    "userInfo": {
        "id": 2970452952,
        "screen_name": "李子柒",
        "profile_image_url": "https://tva1.sinaimg.cn/crop.0.0.750.750.180/b10d83d8jw8f53xpxjlhaj20ku0kut9k.jpg?KID=imgbed,tva&Expires=1587395317&ssig=c0C60Op%2F9D",
        "profile_url": "https://m.weibo.cn/u/2970452952?uid=2970452952&luicode=10000011&lfid=1005052970452952",
        "statuses_count": 402,
        "verified": true,
        "verified_type":0 ,
        "verified_type_ext": 1,
        "verified_reason": "李子柒品牌创始人",
        "close_blue_v": false,
        "description": "邮箱：liziqistyle@163.com",
        "gender": "f",
        "mbtype": 12,
        "urank": 37,
        "mbrank": 6,
        "follow_me": false,
        "following": false,
        "followers_count": 24283132,
        "follow_count": 181,
        "cover_image_phone": "https://tva3.sinaimg.cn/crop.0.0.640.640.640/b10d83d8gw1f6tcqaq92sj20ku0k2dgu.jpg",
        "avatar_hd": "https://ww1.sinaimg.cn/orj480/b10d83d8jw8f53xpxjlhaj20ku0kut9k.jpg",
        "like": false,
        "like_me": false
        }
    }
    """
    id: int = Field(..., alias='platformUID')
    screen_name: str = Field(..., alias='displayName')
    profile_image_url: AnyUrl = Field(..., alias='avatar')
    profile_url: AnyUrl = Field(..., alias='platformLink')
    description: str = Field(..., alias='desc')
    followers_count: int = Field(0, alias='fans')
    statuses_count: int = Field(0, alias='works')
    follow_count: int = Field(0, alias='follows')
    verified: bool = Field(False, alias='isVerify')
    verified_reason: str = Field("", alias='verifyInfo')
    gender: str = Field(..., alias='gender')
    verified_type: str = Field(..., alias='accountType')  # false表示个人号
    mbrank: int = Field(..., alias='accountRank')
    platform: str = 'weibo'
    areaCode: str = 'CN'
    industryType: str = "other"
    industries: List = []

    @root_validator(pre=True)
    def trans_url(cls, values):
        doc = "  ".join([values.get('description', ''), values.get('verified_reason', '')])
        values['industryType'] = getIndustry(doc)
        values['industries'] = [values.get('industryType', "other")]
        values['profile_url'] = ''.join(['https://www.weibo.com/u/{}'.format(values.get('id'))])
        return values

    @validator('verified_type', always=True)
    def trans_verified_type(cls, v):
        # 参考 https://gist.github.com/chen206/7260648
        _DICT = {'-1': "普通用户", "200": "初级达人",
                 "220": "中级达人", "400": "已故V用户",
                 "0": "个人", "1": "政府", "2": "企业", "3": "媒体",
                 "4": "校园", "5": "网站", "6": "应用", "7": "团体"}
        return _DICT.get(v, "")

    @validator('gender', always=True)
    def trans_gender(cls, v):
        GENDER_DICT = {'f': '0', 'm': '1'}
        return GENDER_DICT.get(v, '3')

    def etl(self):
        """ 清洗方法 """
        res = self.dict(by_alias=True)
        return res

    class Config:
        # 允许使用非别名解析对象
        allow_population_by_field_name = True


class XiaohongshuUserInfo(BaseModel):
    """ 小红书用户信息入库
    {
        "bannerImage": "http:\\u002F\\u002Fs4.xiaohongshu.com\\u002Fstatic\\u002Fhuati\\u002F6d46171e066c82db069e0978716269b9.jpg",
        "fans": 1543603,
        "follows": 1,
        "gender": 1,
        "id": "5ab1fd2ee8ac2b56054e07e5",
        "nickname": "李子柒",
        "notes": 304,
        "boards": 1,
        "location": "地球的某一片红薯地",
        "image": "https:\\u002F\\u002Fimg.xiaohongshu.com\\u002Favatar\\u002F5acad07514de41031a1af498.jpg@240w_240h_90q_1e_1c_1x.jpg",
        "images": "https:\\u002F\\u002Fimg.xiaohongshu.com\\u002Favatar\\u002F5acad07514de41031a1af498.jpg@240w_240h_90q_1e_1c_1x.jpg",
        "collected": 222904,
        "desc": "李家有女 人称子柒",
        "liked": 2025059,
        "officialVerified": false,
        "level": {
            "image": "http:\\u002F\\u002Fs4.xiaohongshu.com\\u002Fstatic\\u002Fthrone\\u002F11f_05e45936bee244cb9fafd4768b8f6810.png",
            "name": "金冠薯"
        },
        "fstatus": "none"
    }
    """
    id: str = Field(..., alias='platformUID')
    nickname: str = Field(..., alias='displayName')
    image: AnyUrl = Field(..., alias='avatar')
    desc: str = Field("", alias='desc')
    fans: int = Field(..., alias='fans')
    notes: int = Field(..., alias='works')
    follows: int = Field(..., alias='follows')
    liked: int = Field(..., alias='likes')
    collected: int = Field(..., alias='collected')
    officialVerified: bool = Field(False, alias='isVerify')
    gender: str = Field(..., alias='gender')
    location: str = Field(..., alias='address')
    areaCode: str = Field("CN", alias='areaCode')
    platform: str = 'xiaohongshu'
    platformLink: AnyUrl
    industryType: str = "other"
    industries: List = []

    @root_validator(pre=True)
    def get_industry(cls, values):
        doc = values.get('desc')
        if not doc:
            values['desc'] = ""  # 小红书的desc会出现None
        values['industryType'] = getIndustry(doc)
        values['industries'] = [values.get('industryType', "other")]
        return values

    @root_validator(pre=True)
    def trans_url(cls, values):
        s = values.get("image")
        avatar = s.encode('utf-8').decode('unicode_escape')
        values['image'] = avatar
        values['platformLink'] = "".join(["https://www.xiaohongshu.com/user/profile/", values.get('id')])
        return values

    # 从location中解析出areaCode
    @validator('areaCode', always=True)
    def get_AreaCode(cls, v, values, **kwargs):
        v = values.get('location')
        if not v:
            return ""
        return getAreaCode(v)

    @validator('gender', always=True)
    def trans_gender(cls, v):
        # TODO 需搞清楚小红书的性别取值
        GENDER_DICT = {'1': '0', '0': '1'}
        return GENDER_DICT.get(v, '3')

    def etl(self):
        """ 清洗方法 """
        res = self.dict(by_alias=True)
        return res

    class Config:
        # 允许使用非别名解析对象
        allow_population_by_field_name = True


class DouyinUserInfo(BaseModel):
    """ 抖抖侠数据结构
    {
        "id": "68310389333",
        "uid": "68310389333",
        "short_id": "71158770",
        "nickname": "李子柒",
        "gender": 2,
        "signature": "李家有女，人称子柒",
        "avatar_thumb": {
            "url_list": [
                "https://p9-dy.byteimg.com/aweme/100x100/330b002fd56a93e8b6f1.jpeg",
                "https://p3-dy.byteimg.com/aweme/100x100/330b002fd56a93e8b6f1.jpeg",
                "https://p26-dy.byteimg.com/aweme/100x100/330b002fd56a93e8b6f1.jpeg"
            ]
        },
        "birthday": "",
        "aweme_count": 562,
        "following_count": 1,
        "follower_count": 35375011,
        "favoriting_count": 0,
        "total_favorited": 131636000,
        "location": "绵阳",
        "custom_verify": "美食自媒体",
        "unique_id": "71158770",
        "school_name": "",
        "share_info": {
            "share_url": "www.iesdouyin.com/share/user/68310389333?u_code=15a5jhke2&sec_uid=MS4wLjABAAAAPCnTQLqza4Xqu-uO7KZHcKuILkO7RRz2oapyOC04AQ0"
        },
        "with_commerce_entry": true,
        "enterprise_verify_reason": "",
        "dongtai_count": 553,
        "create_time": 0,
        "province": "四川",
        "cover_url": [
            {
                "url_list": [
                    "https://p9-dy.byteimg.com/obj/c8510002be9a3a61aad2",
                    "https://p29-dy.byteimg.com/obj/c8510002be9a3a61aad2",
                    "https://p3-dy.byteimg.com/obj/c8510002be9a3a61aad2"
                ]
            }
        ],
        "sec_uid": "MS4wLjABAAAAPCnTQLqza4Xqu-uO7KZHcKuILkO7RRz2oapyOC04AQ0",
        "mplatform_followers_count": 40222265,
        "followers_detail": [
            {
                "fans_count": 34905772,
                "name": "抖音"
            },
            {
                "fans_count": 5316493,
                "name": "头条"
            },
            {
                "fans_count": 0,
                "name": "抖音火山版"
            }
        ],
        "country": "中国",
        "city": "绵阳",
        "comment_count": 4648966,
        "share_count": 2419279,
        "checkQualified": false,
        "commerce_user_info": {
            "star_atlas": 1,
            "show_star_atlas_cooperation": false
        },
        "main_category": "美女",
        "vice_categories": [
            "美女",
            "美食",
            "搞笑"
        ]
    }
    """
    uid: str = Field(..., alias='platformUID')
    nickname: str = Field(..., alias='displayName')
    avatar_thumb: AnyUrl = Field(..., alias='avatar')
    share_info: AnyUrl = Field(..., alias='platformLink')
    signature: str = Field(..., alias='desc')
    follower_count: int = Field(..., alias='fans')
    aweme_count: int = Field(..., alias='works')
    following_count: int = Field(..., alias='follows')
    total_favorited: int = Field(..., alias='likes')
    comment_count: int = Field(..., alias='comments')
    custom_verify: str = Field(..., alias='verifyInfo')
    gender: str = Field('3', alias='gender')
    location: str = Field("", alias='address')
    province: str = Field("", alias='province')
    country: str = Field("", alias='country')
    city: str = Field("", alias='city')
    main_category: str = Field(..., alias='industryType')
    vice_categories: list = Field(..., alias='industries')
    platform: str = "douyin"
    areaCode: str = ""
    isVerify: bool = False

    industryType: str = "other"
    industries: List = []

    @root_validator(pre=True)
    def get_industry(cls, values):
        doc = values.get('signature')
        values['industryType'] = getIndustry(doc)
        values['industries'] = [values.get('industryType', "other")] + values.get('vice_categories', [])
        return values

    # 从province中解析出areaCode
    @validator('areaCode', always=True)
    def get_AreaCode(cls, v, values, **kwargs):
        v = values.get('province')
        if not v:
            return ""
        return getAreaCode(v)

    # 通过判断认证信息 得出认证字段值
    @validator('isVerify', always=True)
    def get_isVerify(cls, v, values, **kwargs):
        v = values.get('custom_verify')
        if not v:
            return False
        return True

    # 从avatar_thumb中解析出avatar,share_info解析出platformLink
    @root_validator(pre=True)
    def trans_url(cls, values):
        url_list = values.get('avatar_thumb', {}).get('url_list', [])
        if url_list:
            values['avatar_thumb'] = url_list[0]
        else:
            values[
                'avatar_thumb'] = "https://navigate-network.s3-ap-southeast-1.amazonaws.com/assets/default-avatar.png"
        values['share_info'] = "https://www.iesdouyin.com/share/user/{}".format(values.get('uid'))
        return values

    @validator('gender', always=True)
    def trans_gender(cls, v):
        # TODO 需搞清楚doudouxia对性别的取值
        GENDER_DICT = {'2': '0', '1': '1'}
        return GENDER_DICT.get(v, '3')

    def etl(self):
        """ 清洗方法 """
        res = self.dict(by_alias=True)
        return res

    class Config:
        # 允许使用非别名解析对象
        allow_population_by_field_name = True


class YoutubeUserInfo(BaseModel):
    """ youtube 数据解析
    # 1.将snippet和statistics topicCategories 拉平
    {
    "kind": "youtube#channel",
    "etag": "\"nxOHAKTVB7baOKsQgTtJIyGxcs8/ijsMm2RPJNag_QkRQ0N0r7J1xYE\"",
    "id": "UCoC47do520os_4DBMEFGg4A",
    "snippet": {
        "title": "李子柒 Liziqi",
        "description": "这里是李子柒YouTube官方频道哦～欢迎订阅：https://goo.gl/nkjpSx \nPlease subscribe to 【李子柒 Liziqi 】Liziqi Channel on YouTube if you like my videos\n目前没有任何官方粉丝后援会哦~\nSo far I don't have any official fan clubs~\n\n新浪微博：https://weibo.com/u/2970452952\nFacebook公共主页：https://www.facebook.com/cnliziqi/\nYouTube公共频道：https://www.youtube.com/c/cnliziqi",
        "customUrl": "cnliziqi",
        "publishedAt": "2017-08-22T06:21:56.000Z",
        "thumbnails": {
            "default": {
                "url": "https://yt3.ggpht.com/a/AATXAJwhzp2z3afAm0bK0lmsmVrsS2EdHmGZMVEDhw=s88-c-k-c0xffffffff-no-rj-mo",
                "width": 88,
                "height": 88
            },
            "medium": {
                "url": "https://yt3.ggpht.com/a/AATXAJwhzp2z3afAm0bK0lmsmVrsS2EdHmGZMVEDhw=s240-c-k-c0xffffffff-no-rj-mo",
                "width": 240,
                "height": 240
            },
            "high": {
                "url": "https://yt3.ggpht.com/a/AATXAJwhzp2z3afAm0bK0lmsmVrsS2EdHmGZMVEDhw=s800-c-k-c0xffffffff-no-rj-mo",
                "width": 800,
                "height": 800
            }
        },
        "localized": {
            "title": "李子柒 Liziqi",
            "description": "这里是李子柒YouTube官方频道哦～欢迎订阅：https://goo.gl/nkjpSx \nPlease subscribe to 【李子柒 Liziqi 】Liziqi Channel on YouTube if you like my videos\n目前没有任何官方粉丝后援会哦~\nSo far I don't have any official fan clubs~\n\n新浪微博：https://weibo.com/u/2970452952\nFacebook公共主页：https://www.facebook.com/cnliziqi/\nYouTube公共频道：https://www.youtube.com/c/cnliziqi"
        },
        "country": "CN"
    },
    "contentDetails": {
        "relatedPlaylists": {
            "likes": "LLoC47do520os_4DBMEFGg4A",
            "uploads": "UUoC47do520os_4DBMEFGg4A",
            "watchHistory": "HL",
            "watchLater": "WL"
        }
    },
    "statistics": {
        "viewCount": "1314962787",
        "commentCount": "0",
        "subscriberCount": "9860000",
        "hiddenSubscriberCount": false,
        "videoCount": "107"
    },
    "topicDetails": {
        "topicIds": [
            "/m/02wbm",
            "/m/019_rr",
            "/m/02wbm"
        ],
        "topicCategories": [
            "https://en.wikipedia.org/wiki/Lifestyle_(sociology)",
            "https://en.wikipedia.org/wiki/Food"
        ]
        }
    }
    """

    id: str = Field(..., alias='platformUID')
    title: str = Field(..., alias='displayName')
    thumbnails: AnyUrl = Field(..., alias='avatar')
    description: str = Field(..., alias='desc')
    subscriberCount: int = Field(..., alias='fans')
    videoCount: int = Field(..., alias='works')
    viewCount: int = Field(..., alias='views')
    publishedAt: datetime = Field(..., alias='publishedAt')
    country: str = Field(..., alias='areaCode')
    platform: str = "youtube"
    platformLink: AnyUrl
    industries: List = []
    industryType: str = ""

    @classmethod
    def flattern_values(cls, values: Dict) -> Dict:
        snippet = values.get('snippet')
        statistics = values.get('statistics')
        topicCategories = values.get('topicDetails', {})  # .get('topicCategories', [])
        res = dict(dict(snippet, **statistics), **topicCategories)
        res['id'] = values.get('id')
        return res

    # 从thumbnails中解析出avatar
    @root_validator(pre=True)
    def trans_url(cls, values):
        snippet = values.get('snippet')
        snippet["id"] = values.get('id')
        statistics = values.get('statistics')
        topicCategories = values.get('topicDetails', {})  # .get('topicCategories', [])
        values = dict(dict(snippet, **statistics), **topicCategories)
        values['thumbnails'] = values.get('thumbnails', {}).get('high', {}).get('url')
        values['platformLink'] = "https://www.youtube.com/channel/{}".format(values.get('id'))
        # 解析出行业
        topicCategories = values.get('topicCategories', [])
        if not topicCategories:
            ind = []
            industryType = ""
        else:
            ind = [i.split('/')[-1] for i in topicCategories]
            if len(ind) == 1:
                industryType = ind[0]
            else:
                industryType = ind[1] if ind[0] == "Lifestyle_(sociology)" else ind[0]
        values['industries'] = {mapIndustry(i) for i in ind}
        values['industryType'] = mapIndustry(industryType)

        return values

    def etl(self):
        """ 清洗方法 """
        res = self.dict(by_alias=True)
        return res

    class Config:
        # 允许使用非别名解析对象
        allow_population_by_field_name = True


class InstagramUserInfo(BaseModel):
    """
    Instagram 解析

    {
        "biography": "Official account of Liziqi, so far I don't have any official fan club~",
        "blocked_by_viewer": false,
        "restricted_by_viewer": false,
        "country_block": false,
        "external_url": "https://www.facebook.com/cnliziqi",
        "external_url_linkshimmed": "https://l.instagram.com/?u=https%3A%2F%2Fwww.facebook.com%2Fcnliziqi&e=ATMR0Lh1zcoQoJ-CfnJGLcoOxiyMNG1rK3uk9Say3Zyi-eVr4c1tbKngBpGoDdtJ148zWEeehl3grwsvWg&s=1",
        "edge_followed_by": {
            "count": 338497
        },
        "followed_by_viewer": false,
        "edge_follow": {
            "count": 0
        },
        "follows_viewer": false,
        "full_name": "李子柒",
        "has_ar_effects": false,
        "has_channel": false,
        "has_blocked_viewer": false,
        "highlight_reel_count": 0,
        "has_requested_viewer": false,
        "id": "6688400989",
        "is_business_account": true,
        "is_joined_recently": false,
        "business_category_name": "Creators & Celebrities",
        "category_id": "1602",
        "overall_category_name": null,
        "is_private": false,
        "is_verified": true,
        "edge_mutual_followed_by": {
            "count": 0,
            "edges": []
        },
        "profile_pic_url": "https://scontent-hkg4-1.cdninstagram.com/v/t51.2885-19/s150x150/66978205_2036887856416337_3280539228055797760_n.jpg?_nc_ht=scontent-hkg4-1.cdninstagram.com&_nc_ohc=ykPkh5qxRmkAX-lnOSk&oh=33b318d761cacaeb824286386a7a73af&oe=5EC78B1F",
        "profile_pic_url_hd": "https://scontent-hkg4-1.cdninstagram.com/v/t51.2885-19/s320x320/66978205_2036887856416337_3280539228055797760_n.jpg?_nc_ht=scontent-hkg4-1.cdninstagram.com&_nc_ohc=ykPkh5qxRmkAX-lnOSk&oh=b5633609e571da24993f4c6a4da6923f&oe=5EC80AE7",
        "requested_by_viewer": false,
        "username": "cnliziqi",
        "connected_fb_page": null,
        "edge_felix_video_timeline": {
            "count": 50,
            "page_info": {
                "has_next_page": true,
                "end_cursor": "QVFET1FYWWxTd3FydnJyRHlBUTVUUnhad3A2cWJHS09BWUlWVzBVWWxhR2t5M2dZX2FyS0Y5ZXN3d3RCaG9xLXhYc0hoV1VoaWttLWtydjljYkVJeHB2VA=="
            },
            "edges": []
        },
        "edge_owner_to_timeline_media": {},
        "edge_saved_media": {
            "count": 0,
            "page_info": {
                "has_next_page": false,
                "end_cursor": null
            },
            "edges": []
        },
        "edge_media_collections": {
            "count": 0,
            "page_info": {
                "has_next_page": false,
                "end_cursor": null
            },
            "edges": []
        }
    }
    """
    username: str = Field(..., alias='platformUID')
    full_name: str = Field(..., alias='displayName')
    profile_pic_url: AnyUrl = Field(..., alias='avatar')
    biography: str = Field(..., alias='desc')
    edge_followed_by: int = Field(..., alias='fans')
    edge_felix_video_timeline: int = Field(..., alias='works')
    edge_follow: int = Field(..., alias='follows')
    is_verified: bool = Field(..., alias='isVerify')
    business_category_name: List = Field(..., alias='industries')
    platformLink: AnyUrl
    platform: str = "instagram"

    # 提取信息
    @root_validator(pre=True)
    def trans_info(cls, values):
        values["edge_followed_by"] = values.get('edge_followed_by', {}).get('count')
        values["edge_felix_video_timeline"] = values.get('edge_felix_video_timeline', {}).get('count')
        values["edge_follow"] = values.get('edge_follow', {}).get('count')
        values['platformLink'] = "https://www.instagram.com/{}".format(values.get('username'))
        values['business_category_name'] = values['business_category_name'].split('&')
        return values

    def etl(self):
        """ 清洗方法 """
        res = self.dict(by_alias=True)
        return res

    class Config:
        # 允许使用非别名解析对象
        allow_population_by_field_name = True


class TiktokUserInfo(BaseModel):
    """ tiktok 用户信息解析 userData
    {
        "secUid": "MS4wLjABAAAAEjxOaFdz-EXDYWI2Qz9X6uxMQeQIKaAzkAWyInZ6M309-FuHzhDzDAIyUaVRxLME",
        "userId": "6748953635118187521",
        "isSecret": false,
        "uniqueId": "cnliziqi",
        "nickName": "李子柒",
        "signature": "李家有女，人称子柒\nOfficial Account， welcome subscribe to my YT 李子柒liziqi",
        "covers": [
            "https://p16-tiktokcdn-com.akamaized.net/obj/tiktok-obj/1648254144697346"
        ],
        "coversMedium": [
            "https://p16-tiktokcdn-com.akamaized.net/obj/tiktok-obj/1648254144697346"
        ],
        "following": 0,
        "fans": 871816,
        "heart": "5094594",
        "video": 273,
        "verified": true,
        "digg": 1
    }
    """
    uniqueId: str = Field(..., alias='platformUID')
    nickName: str = Field(..., alias='displayName')
    covers: AnyUrl = Field(..., alias='avatar')
    signature: str = Field(..., alias='desc')
    fans: int = Field(..., alias='fans')
    video: int = Field(..., alias='works')
    following: int = Field(..., alias='follows')
    heart: int = Field(..., alias='likes')
    verified: bool = Field(..., alias='isVerify')
    platformLink: AnyUrl
    platform: str = "tiktok"

    # 提取信息
    @root_validator(pre=True)
    def trans_info(cls, values):
        values['covers'] = values.get('covers', [])[0] if values.get('covers',
                                                                     []) else "https://navigate-network.s3-ap-southeast-1.amazonaws.com/assets/default-avatar.png"
        values['platformLink'] = "https://www.tiktok.com/@{}".format(values.get('uniqueId'))
        return values

    def etl(self):
        """ 清洗方法 """
        res = self.dict(by_alias=True)
        return res

    class Config:
        # 允许使用非别名解析对象
        allow_population_by_field_name = True
