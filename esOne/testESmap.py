# --*-- coding: utf-8 _*_
# @Author   :liyingkun
# @time     :2020/4/23 9:01 下午
# @File     :testESmap.py
# @Software :PyCharm

"""
ES 的mapping定义的使用
"""
from datetime import datetime
from typing import Dict

from elasticsearch import Elasticsearch
from elasticsearch_dsl import Search, tokenizer
from elasticsearch_dsl import connections

client = Elasticsearch(hosts=[{"host": "2dm-dnns.ticp.io", "port": 42836}])

s = Search(using=client)

from elasticsearch_dsl import Document, Date, Nested, Boolean, \
    analyzer, InnerDoc, Completion, Keyword, Text, Integer, AttrDict, AttrList, Long

html_strip = analyzer('html_strip',
                      tokenizer="standard",
                      filter=["standard", "lowercase", "stop", "snowball"],
                      char_filter=["html_strip"]
                      )


class Comment(InnerDoc):
    author = Text(fields={'raw': Keyword()})
    content = Text(analyzer='snowball')
    created_at = Date()

    def age(self):
        return datetime.now() - self.created_at


class Post(Document):
    title = Text()
    title_suggest = Completion()
    created_at = Date()
    published = Boolean()
    category = Text(
        analyzer=html_strip,
        fields={'raw': Keyword()}
    )

    comments = Nested(Comment)

    class Index:
        name = 'blog'

    def add_comment(self, author, content):
        self.comments.append(
            Comment(author=author, content=content, created_at=datetime.now()))

    def save(self, **kwargs):
        self.created_at = datetime.now()
        return super().save(**kwargs)


class Address(InnerDoc):
    country = Keyword()
    province = Keyword()
    city = Keyword()


my_analyzer = analyzer('ik_pinyin_analyzer',
                       type='custom',
                       tokenizer=tokenizer('ik_max_word'),
                       filter=["standard", "lowercase", "stop", "snowball"],
                       char_filter=["html_strip"]
                       )


class SuperSearchInfo(Document):
    kolID = Keyword()
    platform = Keyword()
    platformUID = Keyword()
    displayName = Text(analyzer="smartcn", fields={'raw': Keyword()})
    displayNamePy = Text(analyzer="smartcn", fields={'raw': Keyword()})
    avatar = Keyword()
    platformLink = Keyword()
    desc = Text(analyzer="smartcn", fields={'raw': Keyword()})
    # desc = Text()
    desc_suggest = Completion()
    fans = Integer()
    works = Integer()
    follows = Integer()
    likes = Long()  # 人民日报点赞量 超了Integer类型的上限
    collected = Integer()
    comments = Integer()
    views = Long()  # youtube观看量可能超过Integer的最大值 2**31-1
    exposuresAvg = Integer()
    isVerify = Boolean()
    verifyInfo = Text(analyzer="smartcn", fields={'raw': Keyword()})
    # verifyInfo = Text()
    gender = Keyword()
    age = Integer()
    publishedAt = Date()
    areaCode = Keyword()
    area = Keyword()
    address = Text()
    formatAddr = AttrDict({})
    country = Keyword()
    province = Keyword()
    city = Keyword()
    industries = AttrList([])
    # industry = Keyword()  # 为了兼容navigate 重命名为industryType
    industryType = Keyword()
    email = Keyword()
    phone = Keyword()
    otherContact = Keyword()
    isContact = Boolean()
    accountType = Keyword()
    accountRank = Keyword()
    updateAtUtc = Date()
    updateAtCst = Date()

    class Index:
        name = 'userinfo'


connections.create_connection(alias='default', hosts=[{"host": "2dm-dnns.ticp.io", "port": 42836}], timeout=60)

if __name__ == "__main__":
    SuperSearchInfo.init()
    pass
