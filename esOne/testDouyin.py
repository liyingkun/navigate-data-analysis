# --*-- coding: utf-8 _*_
# @Author   :liyingkun
# @time     :2020/4/22 2:10 下午
# @File     :testDouyin.py
# @Software :PyCharm\
from esOne.models import DouyinUserInfo, SuperSearchUserInfo

douyin = {
    "id": "68310389333",
    "uid": "68310389333",
    "short_id": "71158770",
    "nickname": "李子柒",
    "gender": 2,
    "signature": "李家有女，人称子柒",
    "avatar_thumb": {
        "url_list": [
            "https://p9-dy.byteimg.com/aweme/100x100/330b002fd56a93e8b6f1.jpeg",
            "https://p3-dy.byteimg.com/aweme/100x100/330b002fd56a93e8b6f1.jpeg",
            "https://p26-dy.byteimg.com/aweme/100x100/330b002fd56a93e8b6f1.jpeg"
        ]
    },
    "birthday": "",
    "aweme_count": 562,
    "following_count": 1,
    "follower_count": 35375011,
    "favoriting_count": 0,
    "total_favorited": 131636000,
    "location": "绵阳",
    "custom_verify": "美食自媒体",
    "unique_id": "71158770",
    "school_name": "",
    "share_info": {
        "share_url": "www.iesdouyin.com/share/user/68310389333?u_code=15a5jhke2&sec_uid=MS4wLjABAAAAPCnTQLqza4Xqu-uO7KZHcKuILkO7RRz2oapyOC04AQ0"
    },
    "with_commerce_entry": True,
    "enterprise_verify_reason": "",
    "dongtai_count": 553,
    "create_time": 0,
    "province": "四川",
    "cover_url": [
        {
            "url_list": [
                "https://p9-dy.byteimg.com/obj/c8510002be9a3a61aad2",
                "https://p29-dy.byteimg.com/obj/c8510002be9a3a61aad2",
                "https://p3-dy.byteimg.com/obj/c8510002be9a3a61aad2"
            ]
        }
    ],
    "sec_uid": "MS4wLjABAAAAPCnTQLqza4Xqu-uO7KZHcKuILkO7RRz2oapyOC04AQ0",
    "mplatform_followers_count": 40222265,
    "followers_detail": [
        {
            "fans_count": 34905772,
            "name": "抖音"
        },
        {
            "fans_count": 5316493,
            "name": "头条"
        },
        {
            "fans_count": 0,
            "name": "抖音火山版"
        }
    ],
    "country": "中国",
    "city": "绵阳",
    "comment_count": 4648966,
    "share_count": 2419279,
    "checkQualified": False,
    "commerce_user_info": {
        "star_atlas": 1,
        "show_star_atlas_cooperation": False
    },
    "main_category": "美女",
    "vice_categories": [
        "美女",
        "美食",
        "搞笑"
    ]
}

if __name__ == "__main__":
    res = DouyinUserInfo.parse_obj(douyin)
    info = res.etl()
    userinfo = SuperSearchUserInfo(**info)
    print(userinfo.json(ensure_ascii=False))
