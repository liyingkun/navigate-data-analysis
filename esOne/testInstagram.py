# --*-- coding: utf-8 _*_
# @Author   :liyingkun
# @time     :2020/4/22 3:34 下午
# @File     :testInstagram.py
# @Software :PyCharm
from esOne.models import InstagramUserInfo, SuperSearchUserInfo

ins = {
    "biography": "Official account of Liziqi, so far I don't have any official fan club~",
    "blocked_by_viewer": False,
    "restricted_by_viewer": False,
    "country_block": False,
    "external_url": "https://www.facebook.com/cnliziqi",
    "external_url_linkshimmed": "https://l.instagram.com/?u=https%3A%2F%2Fwww.facebook.com%2Fcnliziqi&e=ATMR0Lh1zcoQoJ-CfnJGLcoOxiyMNG1rK3uk9Say3Zyi-eVr4c1tbKngBpGoDdtJ148zWEeehl3grwsvWg&s=1",
    "edge_followed_by": {
        "count": 338497
    },
    "followed_by_viewer": False,
    "edge_follow": {
        "count": 0
    },
    "follows_viewer": False,
    "full_name": "李子柒",
    "has_ar_effects": False,
    "has_channel": False,
    "has_blocked_viewer": False,
    "highlight_reel_count": 0,
    "has_requested_viewer": False,
    "id": "6688400989",
    "is_business_account": True,
    "is_joined_recently": False,
    "business_category_name": "Creators & Celebrities",
    "category_id": "1602",
    "overall_category_name": None,
    "is_private": False,
    "is_verified": True,
    "edge_mutual_followed_by": {
        "count": 0,
        "edges": []
    },
    "profile_pic_url": "https://scontent-hkg4-1.cdninstagram.com/v/t51.2885-19/s150x150/66978205_2036887856416337_3280539228055797760_n.jpg?_nc_ht=scontent-hkg4-1.cdninstagram.com&_nc_ohc=ykPkh5qxRmkAX-lnOSk&oh=33b318d761cacaeb824286386a7a73af&oe=5EC78B1F",
    "profile_pic_url_hd": "https://scontent-hkg4-1.cdninstagram.com/v/t51.2885-19/s320x320/66978205_2036887856416337_3280539228055797760_n.jpg?_nc_ht=scontent-hkg4-1.cdninstagram.com&_nc_ohc=ykPkh5qxRmkAX-lnOSk&oh=b5633609e571da24993f4c6a4da6923f&oe=5EC80AE7",
    "requested_by_viewer": False,
    "username": "cnliziqi",
    "connected_fb_page": None,
    "edge_felix_video_timeline": {
        "count": 50,
        "page_info": {
            "has_next_page": True,
            "end_cursor": "QVFET1FYWWxTd3FydnJyRHlBUTVUUnhad3A2cWJHS09BWUlWVzBVWWxhR2t5M2dZX2FyS0Y5ZXN3d3RCaG9xLXhYc0hoV1VoaWttLWtydjljYkVJeHB2VA=="
        },
        "edges": []
    },
    "edge_owner_to_timeline_media": {},
    "edge_saved_media": {
        "count": 0,
        "page_info": {
            "has_next_page": False,
            "end_cursor": None
        },
        "edges": []
    },
    "edge_media_collections": {
        "count": 0,
        "page_info": {
            "has_next_page": False,
            "end_cursor": None
        },
        "edges": []
    }
}

if __name__ == "__main__":
    res = InstagramUserInfo(**ins)
    info = res.etl()
    userinfo = SuperSearchUserInfo(**info)
    print(userinfo.json(ensure_ascii=False))
