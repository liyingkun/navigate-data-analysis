# --*-- coding: utf-8 _*_
# @Author   :liyingkun
# @time     :2020/4/22 6:43 下午
# @File     :testCreateIndex.py
# @Software :PyCharm

""" 创建索引 """

from elasticsearch import Elasticsearch

from esOne.models import DouyinUserInfo, SuperSearchUserInfo, InstagramUserInfo, TiktokUserInfo, WeiboUserInfo, \
    XiaohongshuUserInfo, YoutubeUserInfo
from esOne.testDouyin import douyin
from esOne.testInstagram import ins
from esOne.testTiktok import tiktok
from esOne.testWeibo import weibo
from esOne.testXiaohongshu import xiaohongshu
from esOne.testYoutube import youtube

es = Elasticsearch(hosts=[{"host": "2dm-dnns.ticp.io", "port": 42836}])
result = es.indices.create(index='news', ignore=400)
print(result)

# 抖音
res = DouyinUserInfo.parse_obj(douyin)
info = res.etl()
userinfo = SuperSearchUserInfo(**info)
data = userinfo.dict()
result = es.create(index='userinfo', doc_type='politics', id=data.get('kolID'), body=data)
print(result)
# es.index(index='userinfo', doc_type='politics', body=data)

# ins
res = InstagramUserInfo(**ins)
info = res.etl()
userinfo = SuperSearchUserInfo(**info)
data = userinfo.dict()
result = es.create(index='userinfo', doc_type='politics', id=data.get('kolID'), body=data)
print(result)

# tiktok
res = TiktokUserInfo(**tiktok)
info = res.etl()
userinfo = SuperSearchUserInfo(**info)
data = userinfo.dict()
result = es.create(index='userinfo', doc_type='politics', id=data.get('kolID'), body=data)
print(result)

# weibo
res = WeiboUserInfo.parse_obj(weibo)
info = res.etl()
userinfo = SuperSearchUserInfo(**info)
data = userinfo.dict()
result = es.create(index='userinfo', doc_type='politics', id=data.get('kolID'), body=data)
print(result)

# xiaohongshu
res = XiaohongshuUserInfo.parse_obj(xiaohongshu)
info = res.etl()
userinfo = SuperSearchUserInfo(**info)
data = userinfo.dict()
result = es.create(index='userinfo', doc_type='politics', id=data.get('kolID'), body=data)
print(result)

# youtube
youtube = YoutubeUserInfo.flattern_values(youtube)
res = YoutubeUserInfo(**youtube)
info = res.etl()
userinfo = SuperSearchUserInfo(**info)
data = userinfo.dict()
result = es.create(index='userinfo', doc_type='politics', id=data.get('kolID'), body=data)
print(result)

# 查询
result = es.search(index='userinfo', doc_type='politics')
print(result)
