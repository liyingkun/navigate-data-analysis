# --*-- coding: utf-8 _*_
# @Author   :liyingkun
# @time     :2020/5/2 9:08 下午
# @File     :testSavetoES.py
# @Software :PyCharm

"""
写入数据至ES userinfo index中
"""
from esOne.models import DouyinUserInfo, SuperSearchUserInfo, InstagramUserInfo, TiktokUserInfo, WeiboUserInfo, \
    XiaohongshuUserInfo, YoutubeUserInfo
from esOne.testDouyin import douyin
from esOne.testESmap import SuperSearchInfo

from esOne.testInstagram import ins
from esOne.testTiktok import tiktok
from esOne.testWeibo import weibo
from esOne.testXiaohongshu import xiaohongshu
from esOne.testYoutube import youtube

# 抖音

res = DouyinUserInfo.parse_obj(douyin)
info = res.etl()
userinfo = SuperSearchUserInfo(**info)
data = userinfo.dict()
result = SuperSearchInfo(meta={'id': data.get('kolID')}, **data)
result.save()
print(result)
# es.index(index='userinfo', doc_type='politics', body=data)

# ins
res = InstagramUserInfo(**ins)
info = res.etl()
userinfo = SuperSearchUserInfo(**info)
data = userinfo.dict()
result = SuperSearchInfo(meta={'id': data.get('kolID')}, **data)
result.save()

# tiktok
res = TiktokUserInfo(**tiktok)
info = res.etl()
userinfo = SuperSearchUserInfo(**info)
data = userinfo.dict()
result = SuperSearchInfo(meta={'id': data.get('kolID')}, **data)
result.save()

# weibo
res = WeiboUserInfo.parse_obj(weibo)
info = res.etl()
userinfo = SuperSearchUserInfo(**info)
data = userinfo.dict()
result = SuperSearchInfo(meta={'id': data.get('kolID')}, **data)
result.save()

# xiaohongshu
res = XiaohongshuUserInfo.parse_obj(xiaohongshu)
info = res.etl()
userinfo = SuperSearchUserInfo(**info)
data = userinfo.dict()
result = SuperSearchInfo(meta={'id': data.get('kolID')}, **data)
result.save()

# youtube
youtube = YoutubeUserInfo.flattern_values(youtube)
res = YoutubeUserInfo(**youtube)
info = res.etl()
userinfo = SuperSearchUserInfo(**info)
data = userinfo.dict()
result = SuperSearchInfo(meta={'id': data.get('kolID')}, **data)
result.save()
