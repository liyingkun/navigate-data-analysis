# --*-- coding: utf-8 _*_
# @Author   :liyingkun
# @time     :2020/4/22 11:17 上午
# @File     :testXiaohongshu.py
# @Software :PyCharm
from esOne.models import XiaohongshuUserInfo, SuperSearchUserInfo

xiaohongshu = {
    "bannerImage": "http:\\u002F\\u002Fs4.xiaohongshu.com\\u002Fstatic\\u002Fhuati\\u002F6d46171e066c82db069e0978716269b9.jpg",
    "fans": 1543603,
    "follows": 1,
    "gender": 1,
    "id": "5ab1fd2ee8ac2b56054e07e5",
    "nickname": "李子柒",
    "notes": 304,
    "boards": 1,
    "location": "地球的某一片红薯地",
    "image": "https:\\u002F\\u002Fimg.xiaohongshu.com\\u002Favatar\\u002F5acad07514de41031a1af498.jpg@240w_240h_90q_1e_1c_1x.jpg",
    "images": "https:\\u002F\\u002Fimg.xiaohongshu.com\\u002Favatar\\u002F5acad07514de41031a1af498.jpg@240w_240h_90q_1e_1c_1x.jpg",
    "collected": 222904,
    "desc": "李家有女 人称子柒",
    "liked": 2025059,
    "officialVerified": False,
    "level": {
        "image": "http:\\u002F\\u002Fs4.xiaohongshu.com\\u002Fstatic\\u002Fthrone\\u002F11f_05e45936bee244cb9fafd4768b8f6810.png",
        "name": "金冠薯"
    },
    "fstatus": "none"
}

if __name__ == "__main__":
    s = "http:\\u002F\\u002Fs4.xiaohongshu.com\\u002Fstatic\\u002Fthrone\\u002F11f_05e45936bee244cb9fafd4768b8f6810.png"
    s.encode('utf-8').decode('unicode_escape')

    res = XiaohongshuUserInfo.parse_obj(xiaohongshu)
    info = res.etl()
    userinfo = SuperSearchUserInfo(**info)
    print(userinfo.json(ensure_ascii=False))
