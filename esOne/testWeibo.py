# --*-- coding: utf-8 _*_
# @Author   :liyingkun
# @time     :2020/4/21 7:56 下午
# @File     :testWeibo.py
# @Software :PyCharm
from esOne.models import WeiboUserInfo, SuperSearchUserInfo

weibo = {
    "id": 2970452952,
    "screen_name": "李子柒",
    "profile_image_url": "https://tva1.sinaimg.cn/crop.0.0.750.750.180/b10d83d8jw8f53xpxjlhaj20ku0kut9k.jpg?KID=imgbed,tva&Expires=1587395317&ssig=c0C60Op%2F9D",
    "profile_url": "https://m.weibo.cn/u/2970452952?uid=2970452952&luicode=10000011&lfid=1005052970452952",
    "statuses_count": 402,
    "verified": True,
    "verified_type": 0,
    "verified_type_ext": 1,
    "verified_reason": "李子柒品牌创始人",
    "close_blue_v": False,
    "description": "邮箱：liziqistyle@163.com",
    "gender": "f",
    "mbtype": 12,
    "urank": 37,
    "mbrank": 6,
    "follow_me": False,
    "following": False,
    "followers_count": 24283132,
    "follow_count": 181,
    "cover_image_phone": "https://tva3.sinaimg.cn/crop.0.0.640.640.640/b10d83d8gw1f6tcqaq92sj20ku0k2dgu.jpg",
    "avatar_hd": "https://ww1.sinaimg.cn/orj480/b10d83d8jw8f53xpxjlhaj20ku0kut9k.jpg",
    "like": False,
    "like_me": False
}

if __name__ == "__main__":
    # print(WeiboUserInfo.schema(by_alias=True))
    # print(WeiboUserInfo.schema(by_alias=False))
    res = WeiboUserInfo.parse_obj(weibo)
    info = res.etl()
    userinfo = SuperSearchUserInfo(**info)
    print(userinfo.json(ensure_ascii=False))
