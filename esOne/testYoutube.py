# --*-- coding: utf-8 _*_
# @Author   :liyingkun
# @time     :2020/4/22 2:45 下午
# @File     :testYoutube.py
# @Software :PyCharm
from esOne.models import YoutubeUserInfo, SuperSearchUserInfo

youtube = {
    "kind": "youtube#channel",
    "etag": "nxOHAKTVB7baOKsQgTtJIyGxcs8/ijsMm2RPJNag_QkRQ0N0r7J1xYE",
    "id": "UCoC47do520os_4DBMEFGg4A",
    "snippet": {
        "title": "李子柒 Liziqi",
        "description": "这里是李子柒YouTube官方频道哦～欢迎订阅：https://goo.gl/nkjpSx \nPlease subscribe to 【李子柒 Liziqi 】Liziqi Channel on YouTube if you like my videos\n目前没有任何官方粉丝后援会哦~\nSo far I don't have any official fan clubs~\n\n新浪微博：https://weibo.com/u/2970452952\nFacebook公共主页：https://www.facebook.com/cnliziqi/\nYouTube公共频道：https://www.youtube.com/c/cnliziqi",
        "customUrl": "cnliziqi",
        "publishedAt": "2017-08-22T06:21:56.000Z",
        "thumbnails": {
            "default": {
                "url": "https://yt3.ggpht.com/a/AATXAJwhzp2z3afAm0bK0lmsmVrsS2EdHmGZMVEDhw=s88-c-k-c0xffffffff-no-rj-mo",
                "width": 88,
                "height": 88
            },
            "medium": {
                "url": "https://yt3.ggpht.com/a/AATXAJwhzp2z3afAm0bK0lmsmVrsS2EdHmGZMVEDhw=s240-c-k-c0xffffffff-no-rj-mo",
                "width": 240,
                "height": 240
            },
            "high": {
                "url": "https://yt3.ggpht.com/a/AATXAJwhzp2z3afAm0bK0lmsmVrsS2EdHmGZMVEDhw=s800-c-k-c0xffffffff-no-rj-mo",
                "width": 800,
                "height": 800
            }
        },
        "localized": {
            "title": "李子柒 Liziqi",
            "description": "这里是李子柒YouTube官方频道哦～欢迎订阅：https://goo.gl/nkjpSx \nPlease subscribe to 【李子柒 Liziqi 】Liziqi Channel on YouTube if you like my videos\n目前没有任何官方粉丝后援会哦~\nSo far I don't have any official fan clubs~\n\n新浪微博：https://weibo.com/u/2970452952\nFacebook公共主页：https://www.facebook.com/cnliziqi/\nYouTube公共频道：https://www.youtube.com/c/cnliziqi"
        },
        "country": "CN"
    },
    "contentDetails": {
        "relatedPlaylists": {
            "likes": "LLoC47do520os_4DBMEFGg4A",
            "uploads": "UUoC47do520os_4DBMEFGg4A",
            "watchHistory": "HL",
            "watchLater": "WL"
        }
    },
    "statistics": {
        "viewCount": "1314962787",
        "commentCount": "0",
        "subscriberCount": "9860000",
        "hiddenSubscriberCount": False,
        "videoCount": "107"
    },
    "topicDetails": {
        "topicIds": [
            "/m/02wbm",
            "/m/019_rr",
            "/m/02wbm"
        ],
        "topicCategories": [
            "https://en.wikipedia.org/wiki/Lifestyle_(sociology)",
            "https://en.wikipedia.org/wiki/Food"
        ]
    }
}

if __name__ == "__main__":
    youtube = YoutubeUserInfo.flattern_values(youtube)
    res = YoutubeUserInfo(**youtube)
    info = res.etl()
    userinfo = SuperSearchUserInfo(**info)
    print(userinfo.json(ensure_ascii=False))
