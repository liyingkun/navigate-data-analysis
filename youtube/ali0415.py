# --*-- coding: utf-8 _*_
# @Author   :liyingkun
# @time     :2020/4/14 8:02 下午
# @File     :ali0415.py
# @Software :PyCharm

""" 阿里国际 数据需求 分析"""
import pandas as pd
import pymongo
from dfply import X, n, group_by, summarize, summarize_each, full_join, transmute, mutate, distinct
import numpy as np

MONGOURL = 'mongodb://root:dnqW9MriFu5P2x@120.76.101.229:27017/?authSource=admin'
DBNAME = 'navigate-dev'

client = pymongo.MongoClient(MONGOURL)
db = client[DBNAME]

# 查询出数据
"""
1、展示次数和覆盖率 
2、参与度  请将“点赞”和“评论”的总数除以网红的粉丝数量，然后乘以100。
3、点击率(CTR) 
4、品牌曝光
"""

channelIds = ["UCmY3dSr-0TOkJqy0btd2AJg",
              "UCR0AnNR7sViH3TWMJl5jyxw",
              "UCftcLVz-jtPXoH3cWUUDwYw",
              "UCvWWf-LYjaujE50iYai8WgQ",
              "UChnN9MPURwKV2PbEoT2vhTQ",
              "UCFfCqe7b9YiDk2ZiAG8UIGA",
              "UCTzLRZUgelatKZ4nyIKcAbg",
              "UCddiUEpeqJcYeBxX1IVBKvQ",
              "UCT5yFEDO-zY1D1vrPyglmsg",
              "UCFghPtzFcmyDiID1ASMA4Dg",
              "UCSeeUM-1TJjWfxFQfbyg6eA",
              "UCOXFVINC6GCo86LBy0NQCIg",
              "UCgNUeSxwTH5tE9Bgvzek-5Q",
              "UCUt0ZA6l_EidUnBFMR9BZig",
              "UCiM8OI8ySYAoealLy-YRCbg",
              "UCaezsZGhwWgB4ZRmHNCfIyw",
              "UCthGJarCVyno_iRNGCWwTJg",
              "UCpJw7D_T1qAam8XOKh5-mcQ",
              "UCvUCbnwzySKgbKiB_n0O0jQ",
              "UCJ15Zl-v8-ghTgEOTpxHdGw",
              "UChFtIFokGfWATn1Je7Cf6Vg",
              "UC2QfXcj4jVb-kyTJlOl7Mog",
              # "UC-IOf3QFTtxLX-vG9iA4ZtQ", #粉丝数太少 过滤
              "UCHmCEsgZyDZGOKxZMKZjnsg",
              "UC6H07z6zAwbHRl4Lbl0GSsw",
              "UC6-X3epFE06GDWRHLQG_YMA"]

colnames = {'platformUID': "频道ID", 'displayName': "平台昵称",
            'industries': "覆盖行业", 'area': "地区", 'fans': "粉丝数",
            'viewCount': "累计观看量",
            'videoCount': "视频数", 'Participation_mean': "贴均参与度", 'Participation_amax': "历史最佳参与度",
            'retweet_mean': "贴均展示次数", 'retweet_amax': "历史最佳展示次数",
            "viewsprefans_mean": "贴均粉丝观看贡献率", "viewsprefans_amax": "历史最佳粉丝观看贡献率"}


def f_analysis(channelIds):
    ls = []
    for channelId in channelIds:
        try:
            post = db['mid_influencerpost'].find({"platform": "youtube", "platformUID": channelId}, {'_id': 0})

            dfpost = pd.DataFrame(list(post))

            statdf = dfpost >> distinct(X.idPost) >> mutate(Participation=(X.comment + X.like) / X.fans,
                                                            viewsprefans=X.retweet / X.fans) >> \
                     group_by('platformUID') >> \
                     summarize_each([np.mean, np.max], "Participation", "retweet", "viewsprefans")

            statdfdict = statdf.round(4).to_dict(orient="records")[0]

            info = db['mid_influencertwodminfo'].find_one({"platform": "youtube", "platformUID": channelId}, {'_id': 0})
            statistics = info.get('meta').get('statistics')
            statisticsls = {"platformUID": info.get('platformUID'),
                            "displayName": info.get('displayName'),
                            "industries": str(info.get('industries')),
                            "area": info.get('area'),
                            "fans": statistics.get('subscriberCount'),
                            "viewCount": statistics.get('viewCount'),
                            "videoCount": statistics.get('videoCount')}
            df = dict(statisticsls, **statdfdict)
            ls.append(df)
        except Exception as e:
            print(channelId, "====", str(e))
    resdf = pd.DataFrame(ls)
    resdf.rename(columns=colnames, inplace=True)
    return resdf


if __name__ == "__main__":
    resdf = f_analysis(channelIds)
    print(resdf)
    # resdf.to_excel('./ali0415.xlsx', index=False)
    resdf.to_excel('./ali0415_v2.xlsx', index=False)

    """
    # post表 算出参与度 最大 平均
    # 展示次数：将观看量汇总 最大 平均

    channelId = channelIds[0]
    post = db['mid_influencerpost'].find({"platform": "youtube", "platformUID": channelId}, {'_id': 0})

    dfpost = pd.DataFrame(list(post))

    dfpost.columns

    statdf = dfpost >> distinct(X.idPost) >> mutate(Participation=(X.comment + X.like) / X.fans) >> \
             group_by('platformUID') >> \
             summarize_each([np.mean, np.max], "Participation", "retweet")

    statdfdict = statdf.round(4).to_dict(orient="records")[0]

    # 粉丝数 累计观看量 视频量 info表
    # TODO 品牌曝光量 品牌推广帖观看量/累计观看量
    # TODO 品牌曝光率
    # TODO 点击率 平均参与度 [0.38% 左右] 和观看量有关 观看量除以1000 一千个有3.8个点击

    info = db['mid_influencertwodminfo'].find_one({"platform": "youtube", "platformUID": channelId}, {'_id': 0})
    statistics = info.get('meta').get('statistics')
    statisticsls = {"platformUID": info.get('platformUID'),
                    "displayName": info.get('displayName'),
                    "industries": str(info.get('industries')),
                    "area": info.get('area'),
                    "fans": statistics.get('subscriberCount'),
                    "viewCount": statistics.get('viewCount'),
                    "videoCount": statistics.get('videoCount')}
    df = dict(statisticsls, **statdfdict)

    pd.DataFrame([df]).columns
    """
