# --*-- coding: utf-8 _*_
# @Author   :liyingkun
# @time     :2020/7/21 4:54 下午
# @File     :test.py
# @Software :PyCharm
"""
import requests

url = "https://www.tiktok.com/@gemdzq"

payload = {}
headers = {
    'Cookie': 'tt_webid_v2=6802046670671758849'
}

proxies = {
    'http': 'http://128.199.168.66:24000',
    'https': 'http://128.199.168.66:24000',
}
response = requests.request("GET", url, headers=headers, data=payload, proxies=proxies)

print(response.text.encode('utf8'))
"""
import ssl
import urllib.request

ctx = ssl.create_default_context()
ctx.verify_flags = ssl.VERIFY_DEFAULT
opener = urllib.request.build_opener(
    urllib.request.ProxyHandler({'http': 'http://128.199.168.66:24000'}),
    urllib.request.HTTPSHandler(context=ctx))
print(opener.open('https://www.tiktok.com/@gemdzq').read())
