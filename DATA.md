# 各平台KOL基本信息数据汇总情况

- 作者:yinkgun.li@2dm.com.au
- 创建时间: 2020-04-21
- 版本:0.1
- 说明:本版本主要汇总了国内外知名社交平台KOL的用户基本数据

## 以李子柒为例

|平台|主页|
|--|--|
|微博|[https://www.weibo.com/mianyangdanshen](https://www.weibo.com/mianyangdanshen)|
|小红书|[https://www.xiaohongshu.com/user/profile/5ab1fd2ee8ac2b56054e07e5](https://www.xiaohongshu.com/user/profile/5ab1fd2ee8ac2b56054e07e5)|
|抖音|[https://www.iesdouyin.com/share/user/68310389333](https://www.iesdouyin.com/share/user/68310389333)|
|微视|[https://h5.weishi.qq.com/weishi/personal/1521620399014847](https://h5.weishi.qq.com/weishi/personal/1521620399014847)|
|哔哩哔哩|[https://space.bilibili.com/19577966](https://space.bilibili.com/19577966)|
|头条|[https://profile.zjurl.cn/rogue/ugc/profile/?version_code=769&version_name=70609&user_id=52773485452&media_id=1553479483279361&request_source=1&active_tab=dongtai&device_id=65&app_name=news_article](https://profile.zjurl.cn/rogue/ugc/profile/?version_code=769&version_name=70609&user_id=52773485452&media_id=1553479483279361&request_source=1&active_tab=dongtai&device_id=65&app_name=news_article)|
|youtube|[https://www.youtube.com/channel/UCoC47do520os_4DBMEFGg4A](https://www.youtube.com/channel/UCoC47do520os_4DBMEFGg4A)|
|Instagram|[https://www.instagram.com/cnliziqi](https://www.instagram.com/cnliziqi)|
|tiktok|[https://www.tiktok.com/@cnliziqi](https://www.tiktok.com/@cnliziqi)|
|facebook|[https://www.facebook.com/cnliziqi/](https://www.facebook.com/cnliziqi/)|
|twitter|[https://twitter.com/cnliziqi](https://twitter.com/cnliziqi)|

## 字段汇总情况见

详见 [超级搜索-各平台字段整理](https://shimo.im/sheets/ypY899G6rYPt8jvt/MODOC/)



## 获取数据备注

- 微博 容易获取 国内的行业信息 以微博为参考
- 微视 信息最全且较准确 地址可以已此为准
- 小红书
- 哔哩哔哩 几乎没有反爬机制
- 抖音 抓包数据最全 反爬最严重 其实可以考虑 暂时只获取基本信息
- 头条 和抖音能实现账号互通 且爬取头条比爬取抖音简单 可以考虑曲线救国策略

- youtube 走API 国外行业信息 以youtube为参考
- instagram 走后门获取较为容易且一次请求 可获得多种数据
- tiktok 网页端爬取的数据基本就够用 但网页端没有搜索功能 难搜一些


## 各平台数据情况<facebook,twitter>暂无

---
**微博**
```json
{
"userInfo": {
    "id": 2970452952,
    "screen_name": "李子柒",
    "profile_image_url": "https://tva1.sinaimg.cn/crop.0.0.750.750.180/b10d83d8jw8f53xpxjlhaj20ku0kut9k.jpg?KID=imgbed,tva&Expires=1587395317&ssig=c0C60Op%2F9D",
    "profile_url": "https://m.weibo.cn/u/2970452952?uid=2970452952&luicode=10000011&lfid=1005052970452952",
    "statuses_count": 402,
    "verified": true,
    "verified_type":0 ,
    "verified_type_ext": 1,
    "verified_reason": "李子柒品牌创始人",
    "close_blue_v": false,
    "description": "邮箱：liziqistyle@163.com",
    "gender": "f",
    "mbtype": 12,
    "urank": 37,
    "mbrank": 6,
    "follow_me": false,
    "following": false,
    "followers_count": 24283132,
    "follow_count": 181,
    "cover_image_phone": "https://tva3.sinaimg.cn/crop.0.0.640.640.640/b10d83d8gw1f6tcqaq92sj20ku0k2dgu.jpg",
    "avatar_hd": "https://ww1.sinaimg.cn/orj480/b10d83d8jw8f53xpxjlhaj20ku0kut9k.jpg",
    "like": false,
    "like_me": false
    }
}
```
---
---

**小红书**
```json
{
    "userDetail": {
        "bannerImage": "http:\\u002F\\u002Fs4.xiaohongshu.com\\u002Fstatic\\u002Fhuati\\u002F6d46171e066c82db069e0978716269b9.jpg",
        "fans": 1543603,
        "follows": 1,
        "gender": 1,
        "id": "5ab1fd2ee8ac2b56054e07e5",
        "nickname": "李子柒",
        "notes": 304,
        "boards": 1,
        "location": "地球的某一片红薯地",
        "image": "https:\\u002F\\u002Fimg.xiaohongshu.com\\u002Favatar\\u002F5acad07514de41031a1af498.jpg@240w_240h_90q_1e_1c_1x.jpg",
        "images": "https:\\u002F\\u002Fimg.xiaohongshu.com\\u002Favatar\\u002F5acad07514de41031a1af498.jpg@240w_240h_90q_1e_1c_1x.jpg",
        "collected": 222904,
        "desc": "李家有女 人称子柒",
        "liked": 2025059,
        "officialVerified": false,
        "level": {
            "image": "http:\\u002F\\u002Fs4.xiaohongshu.com\\u002Fstatic\\u002Fthrone\\u002F11f_05e45936bee244cb9fafd4768b8f6810.png",
            "name": "金冠薯"
        },
        "fstatus": "none"
    }
}
```
---
---

**抖音**[以抖抖侠为例，其格式接近接口请求获取数据的格式]
```json
{
    "author": {
        "id": "68310389333",
        "uid": "68310389333",
        "short_id": "71158770",
        "nickname": "李子柒",
        "gender": 2,
        "signature": "李家有女，人称子柒",
        "avatar_thumb": {
            "url_list": [
                "https://p9-dy.byteimg.com/aweme/100x100/330b002fd56a93e8b6f1.jpeg",
                "https://p3-dy.byteimg.com/aweme/100x100/330b002fd56a93e8b6f1.jpeg",
                "https://p26-dy.byteimg.com/aweme/100x100/330b002fd56a93e8b6f1.jpeg"
            ]
        },
        "birthday": "",
        "aweme_count": 562,
        "following_count": 1,
        "follower_count": 35375011,
        "favoriting_count": 0,
        "total_favorited": 131636000,
        "location": "绵阳",
        "custom_verify": "美食自媒体",
        "unique_id": "71158770",
        "school_name": "",
        "share_info": {
            "share_url": "www.iesdouyin.com/share/user/68310389333?u_code=15a5jhke2&sec_uid=MS4wLjABAAAAPCnTQLqza4Xqu-uO7KZHcKuILkO7RRz2oapyOC04AQ0"
        },
        "with_commerce_entry": true,
        "enterprise_verify_reason": "",
        "dongtai_count": 553,
        "create_time": 0,
        "province": "四川",
        "cover_url": [
            {
                "url_list": [
                    "https://p9-dy.byteimg.com/obj/c8510002be9a3a61aad2",
                    "https://p29-dy.byteimg.com/obj/c8510002be9a3a61aad2",
                    "https://p3-dy.byteimg.com/obj/c8510002be9a3a61aad2"
                ]
            }
        ],
        "sec_uid": "MS4wLjABAAAAPCnTQLqza4Xqu-uO7KZHcKuILkO7RRz2oapyOC04AQ0",
        "mplatform_followers_count": 40222265,
        "followers_detail": [
            {
                "fans_count": 34905772,
                "name": "抖音"
            },
            {
                "fans_count": 5316493,
                "name": "头条"
            },
            {
                "fans_count": 0,
                "name": "抖音火山版"
            }
        ],
        "country": "中国",
        "city": "绵阳",
        "comment_count": 4648966,
        "share_count": 2419279,
        "checkQualified": false,
        "commerce_user_info": {
            "star_atlas": 1,
            "show_star_atlas_cooperation": false
        },
        "main_category": "美女",
        "vice_categories": [
            "美女",
            "美食",
            "搞笑"
        ]
    },
    "main_category": "美女",
    "follower_count": 35356563,
    "total_favorited": 131536000,
    "follower_increment": 64690,
    "potential": 1351,
    "comment_count": 4643617,
    "share_count": 2416659,
    "aweme_count": 561,
    "nickname": "李子柒",
    "aweme_increment": 0,
    "favorited_increment": 141000,
    "comment_increment": 3899,
    "share_increment": 24,
    "fans_portrayal": {
        "category": {},
        "gender": {},
        "area": {},
        "age": {},
        "age_distributions": {}
    },
    "fans_liveness": {
        "daily": {},
        "weekly": {}
    },
    "hasFixed": true
}
```
---
---

**微视**
```json
{
    "profile": {
        "person": {
            "id": "1521620399014847",
            "type": 2,
            "uid": "AB0D7AB713751E4FB0C61A9A553DD7B5",
            "createtime": 1521620399,
            "nick": "李子柒",
            "avatar": "http://xp.qpic.cn/oscar_pic/0/1047_30393863616364332d343636322pict/480",
            "sex": 0,
            "feedlist_time_id": "1521620399014847:tm",
            "feedlist_hot_id": "1521620399014847:ht",
            "related_feedlist_id": "1521620399014847:rl",
            "followerlist_id": "1521620399014847:flr",
            "interesterlist_id": "1521620399014847:itr",
            "chatlist_id": "1521620399014847:cht",
            "rich_flag": 72,
            "age": 0,
            "address": "四川绵阳",
            "wealth": {
                "flower_num": 0,
                "score": 0
            },
            "background": "",
            "status": "李家有女，人称子柒",
            "followStatus": 2,
            "chartScore": 0,
            "chartRank": -1,
            "feedGoldNum": 0,
            "avatar_updatetime": 1579138898,
            "desc_from_operator": "",
            "sync_content": 0,
            "feedlist_praise_id": "1521620399014847:pre",
            "settingmask": 0,
            "originalavatar": "http://xp.qpic.cn/oscar_pic/0/1047_30393863616364332d343636322pict/0",
            "block_time": "",
            "grade": 2,
            "medal": 4,
            "block_reason": "",
            "qq": 0,
            "recommendReason": "",
            "lastUpdateFeedNum": 0,
            "updateinfo": {
                "flag": 0,
                "tip": "",
                "num": 0
            },
            "nick_updatetime": 1579138898,
            "lastDownloadAvatar": "https://q.qlogo.cn/qqapp/1101083114/AB0D7AB713751E4FB0C61A9A553DD7B5/100",
            "realName": "",
            "pinyin_first": "",
            "certif_desc": "优质美食创作者",
            "privateInfo": {
                "phone_num": "",
                "name": "",
                "id_num": ""
            },
            "extern_info": {
                "mpEx": {
                    "FuncName": "WSApiUpdateDarenData",
                    "audit_priority": "210",
                    "auth_status": "4",
                    "daren_company": "微念科技",
                    "daren_priority": "5",
                    "is_om_onlinesigned_account": "1",
                    "is_omacount": "1",
                    "is_omsigned_account": "1",
                    "media_id": "5668962",
                    "show_yunying": "0",
                    "sub_priority": "0",
                    "yunying_title": "",
                    "yunying_url": ""
                },
                "bind_acct": [],
                "bgPicUrl": "",
                "level_info": {
                    "level": 12,
                    "score": 34309,
                    "prev_upgrade_time": 0
                },
                "weishiId": "",
                "weishiid_modify_count": "",
                "watermark_type": 0,
                "real_nick": "",
                "cmt_level": {
                    "level": 0,
                    "cmtscore": 0,
                    "dingscore": 0,
                    "prev_upgrade_time": 0
                },
                "flexibility_flag": 0,
                "live_status": 0,
                "now_live_room_id": 0,
                "medal_info": {
                    "total_score": 0,
                    "medal_list": []
                },
                "h5_has_login": 0,
                "relation_type": 0
            },
            "certifData": {
                "certif_icon": "",
                "certif_jumpurl": ""
            },
            "isShowPOI": 1,
            "isShowGender": 1,
            "formatAddr": {
                "country": "四川绵阳",
                "province": "",
                "city": ""
            },
            "authorize_time": 0,
            "activityInfo": {
                "invitePersonid": ""
            },
            "special_identity": {}
        },
        "numeric": {
            "feed_num": 77,
            "praise_num": 0,
            "fans_num": 136994,
            "interest_num": 0,
            "receivepraise_num": 42619,
            "is_followed": 0,
            "priv_feed_num": 0,
            "fri_follow_num": 0,
            "frdonly_feed_num": 0,
            "fri_follow_avatar": [],
            "friFollowList": []
        },
        "shareInfo": {
            "jump_url": "https://h5.weishi.qq.com/weishi/personal/1521620399014847/wspersonal?_wv=1&id=1521620399014847&spid=h5",
            "body_map": {
                "0": {
                    "title": "@李子柒的视频都超有趣，根本停不下来！",
                    "desc": "李家有女，人称子柒",
                    "image_url": "http://xp.qpic.cn/oscar_pic/0/1047_30393863616364332d343636322pict/480",
                    "url": ""
                },
                "1": {
                    "title": "@李子柒的视频都超有趣，根本停不下来！",
                    "desc": "李家有女，人称子柒",
                    "image_url": "http://xp.qpic.cn/oscar_pic/0/1047_30393863616364332d343636322pict/480",
                    "url": ""
                },
                "2": {
                    "title": "@李子柒的视频都超有趣，根本停不下来！",
                    "desc": "李家有女，人称子柒",
                    "image_url": "http://xp.qpic.cn/oscar_pic/0/1047_30393863616364332d343636322pict/480",
                    "url": ""
                },
                "3": {
                    "title": "@李子柒的视频都超有趣，根本停不下来！",
                    "desc": "李家有女，人称子柒",
                    "image_url": "http://xp.qpic.cn/oscar_pic/0/1047_30393863616364332d343636322pict/480",
                    "url": ""
                },
                "4": {
                    "title": "@李子柒的视频都超有趣，根本停不下来！",
                    "desc": "",
                    "image_url": "http://xp.qpic.cn/oscar_pic/0/1047_30393863616364332d343636322pict/480",
                    "url": ""
                },
                "5": {
                    "title": "@李子柒的视频都超有趣，根本停不下来！ >>https://h5.weishi.qq.com/weishi/personal/1521620399014847/wspersonal?_wv=1&id=1521620399014847&spid=h5",
                    "desc": "",
                    "image_url": "http://xp.qpic.cn/oscar_pic/0/1047_30393863616364332d343636322pict/480",
                    "url": ""
                }
            },
            "wx_mini_program": {
                "webpageUrl": "",
                "userName": "",
                "path": "",
                "hdImageDataURL": "",
                "withShareTicket": 0,
                "miniProgramType": 0,
                "appid": "",
                "videoUserName": "",
                "videoSource": "",
                "videoCoverWidth": 0,
                "videoCoverHeight": 0,
                "appThumbUrl": "",
                "universalLink": "",
                "disableforward": 0
            },
            "sq_ark_info": {
                "arkData": "",
                "shareBody": {
                    "title": "",
                    "desc": "",
                    "image_url": "",
                    "url": ""
                },
                "coverProto": ""
            },
            "share_icon_url": "",
            "share_icon_title": "",
            "background_url": "",
            "activity_type": 0,
            "haibao_jump_url": "https://h5.qzone.qq.com/weishi/personal/1521620399014847/wspersonal?_proxy=1&_wv=1&id=1521620399014847",
            "haibao_body_map": {
                "0": {
                    "title": "",
                    "desc": "",
                    "image_url": "",
                    "url": ""
                },
                "1": {
                    "title": "",
                    "desc": "",
                    "image_url": "",
                    "url": ""
                },
                "2": {
                    "title": "",
                    "desc": "",
                    "image_url": "",
                    "url": ""
                },
                "3": {
                    "title": "",
                    "desc": "",
                    "image_url": "",
                    "url": ""
                },
                "4": {
                    "title": "",
                    "desc": "",
                    "image_url": "",
                    "url": ""
                },
                "5": {
                    "title": "",
                    "desc": "",
                    "image_url": "",
                    "url": ""
                }
            },
            "background_title_color": "",
            "haibao_desc": "",
            "share_num": 0,
            "feed_cover_updtime": ""
        },
        "collection": {
            "cid": "",
            "name": "",
            "cover": "",
            "desc": "",
            "feedNum": 0,
            "playNum": 0,
            "shareInfo": {
                "jump_url": "",
                "body_map": {},
                "wx_mini_program": {
                    "webpageUrl": "",
                    "userName": "",
                    "path": "",
                    "hdImageDataURL": "",
                    "withShareTicket": 0,
                    "miniProgramType": 0,
                    "appid": "",
                    "videoUserName": "",
                    "videoSource": "",
                    "videoCoverWidth": 0,
                    "videoCoverHeight": 0,
                    "appThumbUrl": "",
                    "universalLink": "",
                    "disableforward": 0
                },
                "sq_ark_info": {
                    "arkData": "",
                    "shareBody": {
                        "title": "",
                        "desc": "",
                        "image_url": "",
                        "url": ""
                    },
                    "coverProto": ""
                },
                "share_icon_url": "",
                "share_icon_title": "",
                "background_url": "",
                "activity_type": 0,
                "haibao_jump_url": "",
                "haibao_body_map": {},
                "background_title_color": "",
                "haibao_desc": "",
                "share_num": 0,
                "feed_cover_updtime": ""
            },
            "attach_info": "",
            "poster": {
                "id": "",
                "type": 0,
                "uid": "",
                "createtime": 0,
                "nick": "",
                "avatar": "",
                "sex": 0,
                "feedlist_time_id": "",
                "feedlist_hot_id": "",
                "related_feedlist_id": "",
                "followerlist_id": "",
                "interesterlist_id": "",
                "chatlist_id": "",
                "rich_flag": 0,
                "age": 0,
                "address": "",
                "wealth": {
                    "flower_num": 0,
                    "score": 0
                },
                "background": "",
                "status": "",
                "followStatus": 0,
                "chartScore": 0,
                "chartRank": -1,
                "feedGoldNum": 0,
                "avatar_updatetime": 0,
                "desc_from_operator": "",
                "sync_content": -1,
                "feedlist_praise_id": "",
                "settingmask": 0,
                "originalavatar": "",
                "block_time": "",
                "grade": 0,
                "medal": 0,
                "block_reason": "",
                "qq": 0,
                "recommendReason": "",
                "lastUpdateFeedNum": 0,
                "updateinfo": {
                    "flag": 0,
                    "tip": "",
                    "num": 0
                },
                "nick_updatetime": 0,
                "lastDownloadAvatar": "",
                "realName": "",
                "pinyin_first": "",
                "certif_desc": "",
                "privateInfo": {
                    "phone_num": "",
                    "name": "",
                    "id_num": ""
                },
                "extern_info": {
                    "mpEx": {},
                    "bind_acct": [],
                    "bgPicUrl": "",
                    "level_info": {
                        "level": 0,
                        "score": 0,
                        "prev_upgrade_time": 0
                    },
                    "weishiId": "",
                    "weishiid_modify_count": "",
                    "watermark_type": 0,
                    "real_nick": "",
                    "cmt_level": {
                        "level": 0,
                        "cmtscore": 0,
                        "dingscore": 0,
                        "prev_upgrade_time": 0
                    },
                    "flexibility_flag": 0,
                    "live_status": 0,
                    "now_live_room_id": 0,
                    "medal_info": {
                        "total_score": 0,
                        "medal_list": []
                    },
                    "h5_has_login": 0,
                    "relation_type": 0
                },
                "certifData": {
                    "certif_icon": "",
                    "certif_jumpurl": ""
                },
                "isShowPOI": 0,
                "isShowGender": 0,
                "formatAddr": {
                    "country": "",
                    "province": "",
                    "city": ""
                },
                "authorize_time": 0,
                "activityInfo": {
                    "invitePersonid": ""
                },
                "special_identity": {}
            },
            "updateTime": 0,
            "updateFeedNum": 0,
            "isFollowed": 0,
            "likeNum": 0
        },
        "itemType": 0
    }
}
```
---
---

**哔哩哔哩**
三次请求：
1. [accountInfo](https://api.bilibili.com/x/space/acc/info?mid=19577966&jsonp=jsonp)
2. [fansinfo](https://api.bilibili.com/x/relation/stat?vmid=19577966&jsonp=jsonp)
3. [upstat](https://api.bilibili.com/x/space/upstat?mid=19577966&jsonp=jsonp)
```json
{
    "mid": 19577966,
    "name": "李子柒",
    "sex": "保密",
    "face": "http://i2.hdslb.com/bfs/face/82d27965dae3b2fe9e52780c6309c7b37ad4cbf2.jpg",
    "sign": "李家有女，人称子柒。 新浪微博：李子柒 邮箱：liziqistyle@163.com",
    "rank": 10000,
    "level": 6,
    "jointime": 0,
    "moral": 0,
    "silence": 0,
    "birthday": "",
    "coins": 0,
    "fans_badge": false,
    "official": {
        "role": 1,
        "title": "bilibili 知名美食UP主",
        "desc": "",
        "type": 0
    },
    "vip": {
        "type": 2,
        "status": 1,
        "theme_type": 0
    },
    "pendant": {
        "pid": 0,
        "name": "",
        "image": "",
        "expire": 0,
        "image_enhance": ""
    },
    "nameplate": {
        "nid": 0,
        "name": "",
        "image": "",
        "image_small": "",
        "level": "",
        "condition": ""
    },
    "is_followed": false,
    "top_photo": "http://i1.hdslb.com/bfs/space/cb1c3ef50e22b6096fde67febe863494caefebad.png",
    "theme": {},
    "sys_notice": {},
    "stat.data": {
        "mid": 19577966,
        "following": 0,
        "whisper": 0,
        "black": 0,
        "follower": 5308834
    },
    "upstat.data":{
         "archive":{"view":151680737},"article":{"view":0},"likes":6114052}
}
```
---
---

**头条** [部分]
```json
{
    "user_info": {
        "avatar_url": "http://p3.pstatp.com/thumb/4ad1000fbc29af838533",
        "follow": false,
        "live_info_type": 0,
        "living_count": 0,
        "name": "李子柒",
        "room_schema": "",
        "user_auth_info": "{\"auth_type\":\"1\",\"auth_info\":\"知名美食领域创作者\",\"other_auth\":{\"interest\":\"知名美食领域创作者\"}}",
        "user_decoration": "",
        "user_id": 52773485452,
        "user_verified": true,
        "verified_content": "知名美食领域创作者"
    }
}
```
---
---

**youtube**
```json
{
    "kind": "youtube#channel",
    "etag": "\"nxOHAKTVB7baOKsQgTtJIyGxcs8/ijsMm2RPJNag_QkRQ0N0r7J1xYE\"",
    "id": "UCoC47do520os_4DBMEFGg4A",
    "snippet": {
        "title": "李子柒 Liziqi",
        "description": "这里是李子柒YouTube官方频道哦～欢迎订阅：https://goo.gl/nkjpSx \nPlease subscribe to 【李子柒 Liziqi 】Liziqi Channel on YouTube if you like my videos\n目前没有任何官方粉丝后援会哦~\nSo far I don't have any official fan clubs~\n\n新浪微博：https://weibo.com/u/2970452952\nFacebook公共主页：https://www.facebook.com/cnliziqi/\nYouTube公共频道：https://www.youtube.com/c/cnliziqi",
        "customUrl": "cnliziqi",
        "publishedAt": "2017-08-22T06:21:56.000Z",
        "thumbnails": {
            "default": {
                "url": "https://yt3.ggpht.com/a/AATXAJwhzp2z3afAm0bK0lmsmVrsS2EdHmGZMVEDhw=s88-c-k-c0xffffffff-no-rj-mo",
                "width": 88,
                "height": 88
            },
            "medium": {
                "url": "https://yt3.ggpht.com/a/AATXAJwhzp2z3afAm0bK0lmsmVrsS2EdHmGZMVEDhw=s240-c-k-c0xffffffff-no-rj-mo",
                "width": 240,
                "height": 240
            },
            "high": {
                "url": "https://yt3.ggpht.com/a/AATXAJwhzp2z3afAm0bK0lmsmVrsS2EdHmGZMVEDhw=s800-c-k-c0xffffffff-no-rj-mo",
                "width": 800,
                "height": 800
            }
        },
        "localized": {
            "title": "李子柒 Liziqi",
            "description": "这里是李子柒YouTube官方频道哦～欢迎订阅：https://goo.gl/nkjpSx \nPlease subscribe to 【李子柒 Liziqi 】Liziqi Channel on YouTube if you like my videos\n目前没有任何官方粉丝后援会哦~\nSo far I don't have any official fan clubs~\n\n新浪微博：https://weibo.com/u/2970452952\nFacebook公共主页：https://www.facebook.com/cnliziqi/\nYouTube公共频道：https://www.youtube.com/c/cnliziqi"
        },
        "country": "CN"
    },
    "contentDetails": {
        "relatedPlaylists": {
            "likes": "LLoC47do520os_4DBMEFGg4A",
            "uploads": "UUoC47do520os_4DBMEFGg4A",
            "watchHistory": "HL",
            "watchLater": "WL"
        }
    },
    "statistics": {
        "viewCount": "1314962787",
        "commentCount": "0",
        "subscriberCount": "9860000",
        "hiddenSubscriberCount": false,
        "videoCount": "107"
    },
    "topicDetails": {
        "topicIds": [
            "/m/02wbm",
            "/m/019_rr",
            "/m/02wbm"
        ],
        "topicCategories": [
            "https://en.wikipedia.org/wiki/Lifestyle_(sociology)",
            "https://en.wikipedia.org/wiki/Food"
        ]
    }
}
```
---
---

**Instagram**
```json
{
    "user": {
        "biography": "Official account of Liziqi, so far I don't have any official fan club~",
        "blocked_by_viewer": false,
        "restricted_by_viewer": false,
        "country_block": false,
        "external_url": "https://www.facebook.com/cnliziqi",
        "external_url_linkshimmed": "https://l.instagram.com/?u=https%3A%2F%2Fwww.facebook.com%2Fcnliziqi&e=ATMR0Lh1zcoQoJ-CfnJGLcoOxiyMNG1rK3uk9Say3Zyi-eVr4c1tbKngBpGoDdtJ148zWEeehl3grwsvWg&s=1",
        "edge_followed_by": {
            "count": 338497
        },
        "followed_by_viewer": false,
        "edge_follow": {
            "count": 0
        },
        "follows_viewer": false,
        "full_name": "李子柒",
        "has_ar_effects": false,
        "has_channel": false,
        "has_blocked_viewer": false,
        "highlight_reel_count": 0,
        "has_requested_viewer": false,
        "id": "6688400989",
        "is_business_account": true,
        "is_joined_recently": false,
        "business_category_name": "Creators & Celebrities",
        "category_id": "1602",
        "overall_category_name": null,
        "is_private": false,
        "is_verified": true,
        "edge_mutual_followed_by": {
            "count": 0,
            "edges": []
        },
        "profile_pic_url": "https://scontent-hkg4-1.cdninstagram.com/v/t51.2885-19/s150x150/66978205_2036887856416337_3280539228055797760_n.jpg?_nc_ht=scontent-hkg4-1.cdninstagram.com&_nc_ohc=ykPkh5qxRmkAX-lnOSk&oh=33b318d761cacaeb824286386a7a73af&oe=5EC78B1F",
        "profile_pic_url_hd": "https://scontent-hkg4-1.cdninstagram.com/v/t51.2885-19/s320x320/66978205_2036887856416337_3280539228055797760_n.jpg?_nc_ht=scontent-hkg4-1.cdninstagram.com&_nc_ohc=ykPkh5qxRmkAX-lnOSk&oh=b5633609e571da24993f4c6a4da6923f&oe=5EC80AE7",
        "requested_by_viewer": false,
        "username": "cnliziqi",
        "connected_fb_page": null,
        "edge_felix_video_timeline": {
            "count": 50,
            "page_info": {
                "has_next_page": true,
                "end_cursor": "QVFET1FYWWxTd3FydnJyRHlBUTVUUnhad3A2cWJHS09BWUlWVzBVWWxhR2t5M2dZX2FyS0Y5ZXN3d3RCaG9xLXhYc0hoV1VoaWttLWtydjljYkVJeHB2VA=="
            },
            "edges": []
        },
        "edge_owner_to_timeline_media": {},
        "edge_saved_media": {
            "count": 0,
            "page_info": {
                "has_next_page": false,
                "end_cursor": null
            },
            "edges": []
        },
        "edge_media_collections": {
            "count": 0,
            "page_info": {
                "has_next_page": false,
                "end_cursor": null
            },
            "edges": []
        }
    }
}
```
---
---

**tiktok**
```json
{
    "userInfo": {
        "user": {
            "id": "6748953635118187521",
            "uniqueId": "cnliziqi",
            "nickname": "李子柒",
            "avatarThumb": "https://p16-tiktokcdn-com.akamaized.net/obj/tiktok-obj/1648254144697346",
            "avatarMedium": "https://p16-tiktokcdn-com.akamaized.net/obj/tiktok-obj/1648254144697346",
            "signature": "李家有女，人称子柒\nOfficial Account， welcome subscribe to my YT 李子柒liziqi",
            "verified": true,
            "secret": false,
            "secUid": "MS4wLjABAAAAEjxOaFdz-EXDYWI2Qz9X6uxMQeQIKaAzkAWyInZ6M309-FuHzhDzDAIyUaVRxLME"
        },
        "stats": {
            "followingCount": 0,
            "followerCount": 871816,
            "heartCount": "5094594",
            "videoCount": 273,
            "diggCount": 1
        }
    },
    "userData": {
        "secUid": "MS4wLjABAAAAEjxOaFdz-EXDYWI2Qz9X6uxMQeQIKaAzkAWyInZ6M309-FuHzhDzDAIyUaVRxLME",
        "userId": "6748953635118187521",
        "isSecret": false,
        "uniqueId": "cnliziqi",
        "nickName": "李子柒",
        "signature": "李家有女，人称子柒\nOfficial Account， welcome subscribe to my YT 李子柒liziqi",
        "covers": [
            "https://p16-tiktokcdn-com.akamaized.net/obj/tiktok-obj/1648254144697346"
        ],
        "coversMedium": [
            "https://p16-tiktokcdn-com.akamaized.net/obj/tiktok-obj/1648254144697346"
        ],
        "following": 0,
        "fans": 871816,
        "heart": "5094594",
        "video": 273,
        "verified": true,
        "digg": 1
    }
}
```
---























